# III. Docker compose

🤖 **Créez un fichier `docker-compose.yml`**

- dans un nouveau dossier dédié `/home/<USER>/compose_test`
```
[robot-045@localhost compose_test]$ cat docker-compose.yml
version: "3"

services:
  conteneur_nul:
    image: debian
    entrypoint: sleep 9999
  conteneur_flopesque:
    image: debian
    entrypoint: sleep 9999
```
- le contenu est le suivant :
```bash
$ docker network create compose_test
$ docker run --name conteneur_nul --network compose_test debian sleep 9999
$ docker run --name conteneur_flopesque --network compose_test debian sleep 9999
```

🤖 **Lancez les deux conteneurs** avec `docker compose`

- go exécuter `docker compose up -d`
```
[robot-045@localhost compose_test]$ docker compose up -d
[+] Running 3/3
 ✔ Network compose_test_default                  Created                                                                                               0.6s
 ✔ Container compose_test-conteneur_flopesque-1  Started                                                                                               0.1s
 ✔ Container compose_test-conteneur_nul-1        Started                                                                                               0.1s
```

🤖 **Vérifier que les deux conteneurs tournent**

- toujours avec une commande `docker`
- tu peux aussi use des trucs comme `docker compose ps` ou `docker compose top` qui sont cools dukoo
```
[robot-045@localhost compose_test]$ docker compose ps
NAME                                 IMAGE     COMMAND        SERVICE               CREATED              STATUS              PORTS
compose_test-conteneur_flopesque-1   debian    "sleep 9999"   conteneur_flopesque   About a minute ago   Up About a minute
compose_test-conteneur_nul-1         debian    "sleep 9999"   conteneur_nul         About a minute ago   Up About a minute
```

🤖 **Pop un shell dans le conteneur `conteneur_nul`**

- référez-vous au mémo Docker
- effectuez un `ping conteneur_flopesque` (ouais ouais, avec ce nom là)
```
[joris@dockertp1linux compose_test]$ docker exec -it compose_test-conteneur_flopesque-1 bash
root@b49430d68b0c:/# ping conteneur_flopesque
bash: ping: command not found
```