# II. Images

- [II. Images](#ii-images)
  - [1. Images publiques](#1-images-publiques)
  - [2. Construire une image](#2-construire-une-image)

## 1. Images publiques

🤖 **Récupérez des images**

- avec la commande `docker pull`
- récupérez :
  - l'image `python` officielle en version 3.11
```
[robot-045@localhost ~]$ docker pull python:3.11
3.11: Pulling from library/python
bc0734b949dc: Already exists
b5de22c0f5cd: Already exists
917ee5330e73: Already exists
b43bd898d5fb: Already exists
7fad4bffde24: Already exists
1f68ce6a3e62: Pull complete
e27d998f416b: Pull complete
fefdcd9854bf: Pull complete
Digest: sha256:4e5e9b05dda9cf699084f20bb1d3463234446387fa0f7a45d90689c48e204c83
Status: Downloaded newer image for python:3.11
docker.io/library/python:3.11
```
  - l'image `mysql` officielle en version 5.7
```
[robot-045@localhost ~]$ docker pull mysql:5.7
5.7: Pulling from library/mysql
20e4dcae4c69: Pull complete
1c56c3d4ce74: Pull complete
e9f03a1c24ce: Pull complete
68c3898c2015: Pull complete
6b95a940e7b6: Pull complete
90986bb8de6e: Pull complete
ae71319cb779: Pull complete
ffc89e9dfd88: Pull complete
43d05e938198: Pull complete
064b2d298fba: Pull complete
df9a4d85569b: Pull complete
Digest: sha256:4bc6bc963e6d8443453676cae56536f4b8156d78bae03c0145cbe47c2aad73bb
Status: Downloaded newer image for mysql:5.7
docker.io/library/mysql:5.7
```
  - l'image `wordpress` officielle en dernière version
```
[robot-045@localhost ~]$ docker pull wordpress:latest
latest: Pulling from library/wordpress
af107e978371: Already exists
6480d4ad61d2: Pull complete
95f5176ece8b: Pull complete
0ebe7ec824ca: Pull complete
673e01769ec9: Pull complete
74f0c50b3097: Pull complete
1a19a72eb529: Pull complete
50436df89cfb: Pull complete
8b616b90f7e6: Pull complete
df9d2e4043f8: Pull complete
d6236f3e94a1: Pull complete
59fa8b76a6b3: Pull complete
99eb3419cf60: Pull complete
22f5c20b545d: Pull complete
1f0d2c1603d0: Pull complete
4624824acfea: Pull complete
79c3af11cab5: Pull complete
e8d8239610fb: Pull complete
a1ff013e1d94: Pull complete
31076364071c: Pull complete
87728bbad961: Pull complete
Digest: sha256:be7173998a8fa131b132cbf69d3ea0239ff62be006f1ec11895758cf7b1acd9e
Status: Downloaded newer image for wordpress:latest
docker.io/library/wordpress:latest
```
  - l'image `linuxserver/wikijs` en dernière version
```
[robot-045@localhost ~]$ docker pull linuxserver/wikijs:latest
latest: Pulling from linuxserver/wikijs
8b16ab80b9bd: Pull complete
07a0e16f7be1: Pull complete
145cda5894de: Pull complete
1a16fa4f6192: Pull complete
84d558be1106: Pull complete
4573be43bb06: Pull complete
20b23561c7ea: Pull complete
Digest: sha256:131d247ab257cc3de56232b75917d6f4e24e07c461c9481b0e7072ae8eba3071
Status: Downloaded newer image for linuxserver/wikijs:latest
docker.io/linuxserver/wikijs:latest
```
- listez les images que vous avez sur la machine avec une commande `docker`
```
[robot-045@localhost ~]$ docker images
REPOSITORY           TAG       IMAGE ID       CREATED       SIZE
linuxserver/wikijs   latest    869729f6d3c5   6 days ago    441MB
mysql                5.7       5107333e08a8   9 days ago    501MB
wordpress            latest    fd2f5a0c6fba   2 weeks ago   739MB
python               3.11      22140cbb3b0c   2 weeks ago   1.01GB
```

🤖 **Lancez un conteneur à partir de l'image Python**

- lancez un terminal `bash` ou `sh`
```
[robot-045@localhost ~]$ docker run -it python:3.11 bash
```
- vérifiez que la commande `python` est installée dans la bonne version
```
root@d5dd932d049a:/# python --version
Python 3.11.7
```

## 2. Construire une image

Pour construire une image il faut :

- créer un fichier `Dockerfile`
- exécuter une commande `docker build` pour produire une image à partir du `Dockerfile`
```
[robot-045@localhost ~]$ nano Dockerfile
```

🤖 **Ecrire un Dockerfile pour une image qui héberge une application Python**

- l'image doit contien
```
[robot-045@localhost python_app_build]$ cat Dockerfile
# Utiliser une base Debian
FROM debian

# Mise à jour des paquets disponibles
RUN apt update && apt upgrade -y

# Installation de Python
RUN apt install -y python3 python3-pip

# Installation de la librairie Python emoji
RUN apt install python3-emoji

# Copie de l'application dans le conteneur
COPY app.py /app.py

# Définir le répertoire de travail
WORKDIR /

# Commande à exécuter lorsqu'un conteneur basé sur cette image est démarré
ENTRYPOINT ["python3", "/app.py"]

```

- pour faire ça, créez un dossier `python_app_build`
```
[robot-045@localhost ~]$ mkdir python_app_build
```
  - dedans, tu mets le code dans un fichier `app.py`
```
[robot-045@localhost python_app_build]$ nano app.py
[robot-045@localhost python_app_build]$ cat app.py
import emoji

print(emoji.emojize("Cet exemple d'application est vraiment naze :thumbs_down:"))
```
  - tu mets aussi `le Dockerfile` dedans
```
[robot-045@localhost python_app_build]$ ls
app.py  Dockerfile
```

🤖 **Build l'image**

- déplace-toi dans ton répertoire de build `cd python_app_build`
```
[robot-045@localhost ~]$ cd ~/python_app_build
```
- `docker build . -t python_app:version_de_ouf`
```
[robot-045@localhost ~]$ docker image ls | grep app
python_app           version_de_ouf   fc7a60e86bae   About a minute ago   635MB
```
- une fois le build terminé, constater que l'image est dispo avec une commande `docker`

🤖 **Lancer l'image**

- lance l'image avec `docker run` :

```
[robot-045@localhost ~]$ docker run python_app:version_de_ouf
Cet exemple d'application est vraiment naze 👎
```
