# I. Init

- [I. Init](#i-init)
  - [1. Installation de Docker](#1-installation-de-docker)
  - [2. Vérifier que Docker est bien là](#2-vérifier-que-docker-est-bien-là)
  - [3. sudo c pa bo](#3-sudo-c-pa-bo)
  - [4. Un premier conteneur en vif](#4-un-premier-conteneur-en-vif)
  - [5. Un deuxième conteneur en vif](#5-un-deuxième-conteneur-en-vif)

## 1. Installation de Docker

```
sudo dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
sudo dnf install docker-ce docker-ce-cli containerd.io
sudo systemctl enable docker
sudo systemctl start docker
sudo usermod -aG docker $USER
```
 
## 2. Vérifier que Docker est bien là

## 3. sudo c pa bo

🤖 **Ajouter votre utilisateur au groupe `docker`**

```
[robot-045@localhost ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

## 4. Un premier conteneur en vif

🤖 **Lancer un conteneur NGINX**

```
[robot-045@localhost ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS                                   NAMES
b15d63fbdde0   nginx     "/docker-entrypoint.…"   8 seconds ago   Up 7 seconds   0.0.0.0:9999->80/tcp, :::9999->80/tcp   angry_robinson
```

🤖 **Visitons**

- vérifier que le conteneur est actif avec une commande qui liste les conteneurs en cours de fonctionnement
```
[robot-045@localhost ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED              STATUS              PORTS                                   NAMES
b15d63fbdde0   nginx     "/docker-entrypoint.…"   About a minute ago   Up About a minute   0.0.0.0:9999->80/tcp, :::9999->80/tcp   angry_robinson
```
- afficher les logs du conteneur
```
[robot-045@localhost ~]$ docker logs angry_robinson
/docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
/docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
/docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
10-listen-on-ipv6-by-default.sh: info: Getting the checksum of /etc/nginx/conf.d/default.conf
10-listen-on-ipv6-by-default.sh: info: Enabled listen on IPv6 in /etc/nginx/conf.d/default.conf
/docker-entrypoint.sh: Sourcing /docker-entrypoint.d/15-local-resolvers.envsh
/docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
/docker-entrypoint.sh: Launching /docker-entrypoint.d/30-tune-worker-processes.sh
/docker-entrypoint.sh: Configuration complete; ready for start up
2023/12/21 10:29:56 [notice] 1#1: using the "epoll" event method
2023/12/21 10:29:56 [notice] 1#1: nginx/1.25.3
2023/12/21 10:29:56 [notice] 1#1: built by gcc 12.2.0 (Debian 12.2.0-14)
2023/12/21 10:29:56 [notice] 1#1: OS: Linux 4.18.0-513.9.1.el8_9.x86_64
2023/12/21 10:29:56 [notice] 1#1: getrlimit(RLIMIT_NOFILE): 1048576:1048576
2023/12/21 10:29:56 [notice] 1#1: start worker processes
2023/12/21 10:29:56 [notice] 1#1: start worker process 29
2023/12/21 10:29:56 [notice] 1#1: start worker process 30
```
- afficher toutes les informations relatives au conteneur avec une commande `docker inspect`
```
[robot-045@localhost ~]$ docker inspect angry_robinson
[
    {
        "Id": "b15d63fbdde0be3857a885c92155009b207bec08a6c2bc7c46dac885298a6af9",
        "Created": "2023-12-21T10:29:55.283131678Z",
        "Path": "/docker-entrypoint.sh",
        "Args": [
            "nginx",
            "-g",
            "daemon off;"
        ],
        "State": {
            "Status": "running",
            "Running": true,
            "Paused": false,
            "Restarting": false,
            "OOMKilled": false,
            "Dead": false,
            "Pid": 1912,
            "ExitCode": 0,
            "Error": "",
            "StartedAt": "2023-12-21T10:29:56.126616403Z",
            "FinishedAt": "0001-01-01T00:00:00Z"
        },
        "Image": "sha256:d453dd892d9357f3559b967478ae9cbc417b52de66b53142f6c16c8a275486b9",
        "ResolvConfPath": "/var/lib/docker/containers/b15d63fbdde0be3857a885c92155009b207bec08a6c2bc7c46dac885298a6af9/resolv.conf",
        "HostnamePath": "/var/lib/docker/containers/b15d63fbdde0be3857a885c92155009b207bec08a6c2bc7c46dac885298a6af9/hostname",
        "HostsPath": "/var/lib/docker/containers/b15d63fbdde0be3857a885c92155009b207bec08a6c2bc7c46dac885298a6af9/hosts",
        "LogPath": "/var/lib/docker/containers/b15d63fbdde0be3857a885c92155009b207bec08a6c2bc7c46dac885298a6af9/b15d63fbdde0be3857a885c92155009b207bec08a6c2bc7c46dac885298a6af9-json.log",
        "Name": "/angry_robinson",
        "RestartCount": 0,
        "Driver": "overlay2",
        "Platform": "linux",
        "MountLabel": "",
        "ProcessLabel": "",
        "AppArmorProfile": "",
        "ExecIDs": null,
        "HostConfig": {
            "Binds": null,
            "ContainerIDFile": "",
            "LogConfig": {
                "Type": "json-file",
                "Config": {}
            },
            "NetworkMode": "default",
            "PortBindings": {
                "80/tcp": [
                    {
                        "HostIp": "",
                        "HostPort": "9999"
                    }
                ]
            },
            "RestartPolicy": {
                "Name": "no",
                "MaximumRetryCount": 0
            },
            "AutoRemove": false,
            "VolumeDriver": "",
            "VolumesFrom": null,
            "ConsoleSize": [
                34,
                120
            ],
            "CapAdd": null,
            "CapDrop": null,
            "CgroupnsMode": "host",
            "Dns": [],
            "DnsOptions": [],
            "DnsSearch": [],
            "ExtraHosts": null,
            "GroupAdd": null,
            "IpcMode": "private",
            "Cgroup": "",
            "Links": null,
            "OomScoreAdj": 0,
            "PidMode": "",
            "Privileged": false,
            "PublishAllPorts": false,
            "ReadonlyRootfs": false,
            "SecurityOpt": null,
            "UTSMode": "",
            "UsernsMode": "",
            "ShmSize": 67108864,
            "Runtime": "runc",
            "Isolation": "",
            "CpuShares": 0,
            "Memory": 0,
            "NanoCpus": 0,
            "CgroupParent": "",
            "BlkioWeight": 0,
            "BlkioWeightDevice": [],
            "BlkioDeviceReadBps": [],
            "BlkioDeviceWriteBps": [],
            "BlkioDeviceReadIOps": [],
            "BlkioDeviceWriteIOps": [],
            "CpuPeriod": 0,
            "CpuQuota": 0,
            "CpuRealtimePeriod": 0,
            "CpuRealtimeRuntime": 0,
            "CpusetCpus": "",
            "CpusetMems": "",
            "Devices": [],
            "DeviceCgroupRules": null,
            "DeviceRequests": null,
            "MemoryReservation": 0,
            "MemorySwap": 0,
            "MemorySwappiness": null,
            "OomKillDisable": false,
            "PidsLimit": null,
            "Ulimits": null,
            "CpuCount": 0,
            "CpuPercent": 0,
            "IOMaximumIOps": 0,
            "IOMaximumBandwidth": 0,
            "MaskedPaths": [
                "/proc/asound",
                "/proc/acpi",
                "/proc/kcore",
                "/proc/keys",
                "/proc/latency_stats",
                "/proc/timer_list",
                "/proc/timer_stats",
                "/proc/sched_debug",
                "/proc/scsi",
                "/sys/firmware",
                "/sys/devices/virtual/powercap"
            ],
            "ReadonlyPaths": [
                "/proc/bus",
                "/proc/fs",
                "/proc/irq",
                "/proc/sys",
                "/proc/sysrq-trigger"
            ]
        },
        "GraphDriver": {
            "Data": {
                "LowerDir": "/var/lib/docker/overlay2/92d2a7ac339bc29fd0d38dd72b49b1add91433c9296c3497537badd5ebab17e1-init/diff:/var/lib/docker/overlay2/4dfa939c640383262def074cc17e4d6e7ceeac50e917737a999bfcdabe8ec3dc/diff:/var/lib/docker/overlay2/cdfac0ed65173ac72011aae3fd863a7ae29c3b109836b078e034f1525069ffb9/diff:/var/lib/docker/overlay2/f144c31313404e5486da80ebe0d56731a0ca191de782a80a8abe7cd18d4f3dbf/diff:/var/lib/docker/overlay2/64ff960d523b7bcc98373db127a248d250dd0fd53e11fcd262406d7d37ecfc4e/diff:/var/lib/docker/overlay2/963dc3b95c1ab8bc1a301ad81c95fea22638d16324784290a5096eab7445787b/diff:/var/lib/docker/overlay2/94c4409c1ec53c5e03c4fa90d9bd8390600cb09ac4cc857da0e1ddde250933e7/diff:/var/lib/docker/overlay2/3359ba633c71ce61aecabeba3ba1ebd4765f62861c0bed6dc5c75050360150bc/diff",
                "MergedDir": "/var/lib/docker/overlay2/92d2a7ac339bc29fd0d38dd72b49b1add91433c9296c3497537badd5ebab17e1/merged",
                "UpperDir": "/var/lib/docker/overlay2/92d2a7ac339bc29fd0d38dd72b49b1add91433c9296c3497537badd5ebab17e1/diff",
                "WorkDir": "/var/lib/docker/overlay2/92d2a7ac339bc29fd0d38dd72b49b1add91433c9296c3497537badd5ebab17e1/work"
            },
            "Name": "overlay2"
        },
        "Mounts": [],
        "Config": {
            "Hostname": "b15d63fbdde0",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": false,
            "AttachStderr": false,
            "ExposedPorts": {
                "80/tcp": {}
            },
            "Tty": false,
            "OpenStdin": false,
            "StdinOnce": false,
            "Env": [
                "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
                "NGINX_VERSION=1.25.3",
                "NJS_VERSION=0.8.2",
                "PKG_RELEASE=1~bookworm"
            ],
            "Cmd": [
                "nginx",
                "-g",
                "daemon off;"
            ],
            "Image": "nginx",
            "Volumes": null,
            "WorkingDir": "",
            "Entrypoint": [
                "/docker-entrypoint.sh"
            ],
            "OnBuild": null,
            "Labels": {
                "maintainer": "NGINX Docker Maintainers <docker-maint@nginx.com>"
            },
            "StopSignal": "SIGQUIT"
        },
        "NetworkSettings": {
            "Bridge": "",
            "SandboxID": "e7545cd5da8b75a5ef8abaa71f06592adb185d70c4ed34c96b618b0b4ef511ed",
            "HairpinMode": false,
            "LinkLocalIPv6Address": "",
            "LinkLocalIPv6PrefixLen": 0,
            "Ports": {
                "80/tcp": [
                    {
                        "HostIp": "0.0.0.0",
                        "HostPort": "9999"
                    },
                    {
                        "HostIp": "::",
                        "HostPort": "9999"
                    }
                ]
            },
            "SandboxKey": "/var/run/docker/netns/e7545cd5da8b",
            "SecondaryIPAddresses": null,
            "SecondaryIPv6Addresses": null,
            "EndpointID": "2c271453e54014c94c6a613420733742e89826b3d6636518a81c62a0c01431d5",
            "Gateway": "172.17.0.1",
            "GlobalIPv6Address": "",
            "GlobalIPv6PrefixLen": 0,
            "IPAddress": "172.17.0.2",
            "IPPrefixLen": 16,
            "IPv6Gateway": "",
            "MacAddress": "02:42:ac:11:00:02",
            "Networks": {
                "bridge": {
                    "IPAMConfig": null,
                    "Links": null,
                    "Aliases": null,
                    "NetworkID": "d301ecc27692308e61a33e4abd4f2be30b4cb48973a93c2918ca7341c18a008e",
                    "EndpointID": "2c271453e54014c94c6a613420733742e89826b3d6636518a81c62a0c01431d5",
                    "Gateway": "172.17.0.1",
                    "IPAddress": "172.17.0.2",
                    "IPPrefixLen": 16,
                    "IPv6Gateway": "",
                    "GlobalIPv6Address": "",
                    "GlobalIPv6PrefixLen": 0,
                    "MacAddress": "02:42:ac:11:00:02",
                    "DriverOpts": null
                }
            }
        }
    }
]
```
- afficher le port en écoute sur la VM avec un `sudo ss -lnpt`
```
[robot-045@localhost ~]$ sudo ss -lnpt
[sudo] password for robot-045:
State         Recv-Q        Send-Q               Local Address:Port                 Peer Address:Port        Process
LISTEN        0             128                        0.0.0.0:22                        0.0.0.0:*            users:(("sshd",pid=712,fd=3))
LISTEN        0             2048                       0.0.0.0:9999                      0.0.0.0:*            users:(("docker-proxy",pid=1872,fd=4))
LISTEN        0             128                           [::]:22                           [::]:*            users:(("sshd",pid=712,fd=4))
LISTEN        0             2048                          [::]:9999                         [::]:*            users:(("docker-proxy",pid=1878,fd=4))
```
- ouvrir le port `9999/tcp` (vu dans le `ss` au dessus normalement) dans le firewall de la VM
```
[robot-045@localhost ~]$ sudo firewall-cmd --list-ports
9999/tcp
```
- depuis le navigateur de votre PC, visiter le site web sur `http://IP_VM:9999`
```
http://10.1.30.1:9999
```

- en effet, il est possible de partager un fichier ou un dossier avec un conteneur, au moment où on le lance
- avec NGINX par exemple, c'est idéal pour déposer un fichier de conf différent à chaque conteneur NGINX qu'on lance
  - en plus NGINX inclut par défaut tous les fichiers dans `/etc/nginx/conf.d/*.conf`
  - donc suffit juste de drop un fichier là-bas
- ça se fait avec `-v` pour *volume* (on appelle ça "monter un volume")

🤖 **On va ajouter un site Web au conteneur NGINX**

- créez un dossier `nginx`
```
[robot-045@localhost ~]$ mkdir ~/nginx
[robot-045@localhost nginx]$ sudo nano site_nul.conf
```

- dedans, deux fichiers : `index.html` (un site nul) `site_nul.conf` (la conf NGINX de notre site nul)
```
[robot-045@localhost nginx]$ sudo nano index.html
[robot-045@localhost nginx]$ sudo cat index.html
<!DOCTYPE html>
<html>
        <h1>MEOOOW</h1>
</html>
```

- config NGINX minimale pour servir un nouveau site web dans `site_nul.conf` :
```
[robot-045@localhost nginx]$ sudo cat site_nul.conf
server {
    listen 8080;

    location / {
        root /var/www/html;
    }
}
```

- lancez le conteneur avec la commande en dessous, notez que :
```
[robot-045@localhost nginx]$ docker run -d -p 8888:8080 -v /home/robot-045/nginx/index.html:/var/www/html/index.html -v /home/robot-045/nginx/site_nul.conf:/etc/nginx/conf.d/site_nul.conf nginx
39d7293c2d4992886be87c96134d3157c06eb2f96e23005b7bb0e261463d9827
docker: Error response from daemon: driver failed programming external connectivity on endpoint zealous_mahavira (796e7c9a7a9e3002bb072486ceee0ef1312a57ae3c65949f8d020d637e4695ed): Bind for 0.0.0.0:8888 failed: port is already allocated.
```

🤖 **Visitons**

- vérifier que le conteneur est actif
```
[robot-045@localhost nginx]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED          STATUS          PORTS                                               NAMES
3a6c0f604c79   nginx     "/docker-entrypoint.…"   4 minutes ago    Up 4 minutes    80/tcp, 0.0.0.0:8888->8080/tcp, :::8888->8080/tcp   nervous_lichterman
b15d63fbdde0   nginx     "/docker-entrypoint.…"   24 minutes ago   Up 24 minutes   0.0.0.0:9999->80/tcp, :::9999->80/tcp               angry_robinson
```

## 5. Un deuxième conteneur en vif

🤖 **Lance un conteneur Python, avec un shell**

```
[robot-045@localhost ~]$ docker run -it python bash
```

🤖 **Installe des libs Python**

```
pip install aiohttp aioconsole
```
```
root@84499f508132:/# python
```
```
>>> import aiohttp
```