# TP3 Admin : Vagrant

## Sommaire

- [TP3 Admin : Vagrant](#tp3-admin--vagrant)
  - [Sommaire](#sommaire)
  - [0. Intro blabla](#0-intro-blabla)
  - [1. Une première VM](#1-une-première-vm)
  - [2. Repackaging](#2-repackaging)
  - [3. Moult VMs](#3-moult-vms)

---
## 0. Intro blabla

---
## 1. Une première VM

🤖 **Générer un `Vagrantfile`**

- vous bosserez avec cet OS pour le restant du TP
- vous pouvez générer une `Vagrantfile` fonctionnel pour une box donnée avec les commandes :

```
PS C:\Users\lanfr\Cours Ynov\B2\b2-reseau\TP-Linux\TP-3> mkdir vagrant
PS C:\Users\lanfr\Cours Ynov\B2\b2-reseau\TP-Linux\TP-3> cd .\vagrant\
PS C:\Users\lanfr\Cours Ynov\B2\b2-reseau\TP-Linux\TP-3\vagrant> mkdir test
PS C:\Users\lanfr\Cours Ynov\B2\b2-reseau\TP-Linux\TP-3\vagrant> cd .\test\
PS C:\Users\lanfr\Cours Ynov\B2\b2-reseau\TP-Linux\TP-3\vagrant\test> vagrant init generic/ubuntu2204
ready to `vagrant up` your first virtual environment! Please read
the comments in the Vagrantfile as well as documentation on
`vagrantup.com` for more information on using Vagrant.
```
```ruby
PS C:\Users\lanfr\Cours Ynov\B2\b2-reseau\TP-Linux\TP-3\vagrant\test> cat .\Vagrantfile
# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "generic/ubuntu2204"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Disable the default share of the current code directory. Doing this
  # provides improved isolation between the vagrant box and your host
  # by making sure your Vagrantfile isn't accessable to the vagrant box.
  # If you use this you may want to enable additional shared subfolders as
  # shown above.
  # config.vm.synced_folder ".", "/vagrant", disabled: true

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL
end
```

🤖 **Modifier le `Vagrantfile`**

- les lignes qui suivent doivent être ajouter dans le bloc où l'objet `config` est défini
- ajouter les lignes suivantes :

```ruby
PS C:\Users\lanfr\Cours Ynov\B2\b2-reseau\TP-Linux\TP-3\vagrant\test> cat .\Vagrantfile
# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "generic/ubuntu2204"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Disable the default share of the current code directory. Doing this
  # provides improved isolation between the vagrant box and your host
  # by making sure your Vagrantfile isn't accessable to the vagrant box.
  # If you use this you may want to enable additional shared subfolders as
  # shown above.
  config.vm.synced_folder ".", "/vagrant", disabled: true

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL
end

```

🤖 **Faire joujou avec une VM**

```bash
PS C:\Users\lanfr\Cours Ynov\B2\b2-reseau\TP-Linux\TP-3\vagrant\test> vagrant ssh
vagrant@ubuntu2204:~$
```

---
## 2. Repackaging

🤖 **Repackager la box que vous avez choisie**

- elle doit :
  - être à jour
  - disposer des commandes `vim`, `ip`, `dig`, `ss`, `nc`
  - avoir un firewall actif
  - SELinux (systèmes RedHat) et/ou AppArmor (plutôt sur Ubuntu) désactivés
- pour repackager une box, vous pouvez utiliser les commandes suivantes :

```bash
PS C:\Users\lanfr\Cours Ynov\B2\b2-reseau\TP-Linux\TP-3\vagrant\test> vagrant package --output super_box.box
==> default: Attempting graceful shutdown of VM...
==> default: Clearing any previously set forwarded ports...
==> default: Exporting VM...
==> default: Compressing package to: C:/Users/lanfr/Cours Ynov/B2/b2-reseau/TP-Linux/TP-3/vagrant/test/super_box.box
PS C:\Users\lanfr\Cours Ynov\B2\b2-reseau\TP-Linux\TP-3\vagrant\test> vagrant box add super_box super_box.box
==> box: Box file was not detected as metadata. Adding it directly...
==> box: Adding box 'super_box' (v0) for provider:
    box: Unpacking necessary files from: file://C:/Users/lanfr/Cours%20Ynov/B2/b2-reseau/TP-Linux/TP-3/vagrant/test/super_box.box
    box:
==> box: Successfully added box 'super_box' (v0) for ''!
PS C:\Users\lanfr\Cours Ynov\B2\b2-reseau\TP-Linux\TP-3\vagrant\test> vagrant box list
geerlingguy/rockylinux8 (virtualbox, 1.0.1)
generic/ubuntu2204      (virtualbox, 4.3.2)
super_box               (virtualbox, 0)
```

🤖 **Ecrivez un `Vagrantfile` qui lance une VM à partir de votre Box**

```bash
PS C:\Users\lanfr\Cours Ynov\B2\b2-reseau\TP-Linux\TP-3\vagrant\test> vagrant status
Current machine states:

default                   running (virtualbox)
```

---
## 3. Moult VMs

🤖 **Adaptez votre `Vagrantfile`** pour qu'il lance les VMs suivantes (en réutilisant votre box de la partie précédente)

- vous devez utiliser une boucle for dans le `Vagrantfile`
- pas le droit de juste copier coller le même bloc trois fois, une boucle for j'ai dit !

| Name           | IP locale   | Accès internet | RAM |
| -------------- | ----------- | -------------- | --- |
| `node1.tp3.b2` | `10.3.1.11` | Ui             | 1G  |
| `node2.tp3.b2` | `10.3.1.12` | Ui             | 1G  |
| `node3.tp3.b2` | `10.3.1.13` | Ui             | 1G  |

- 📂 fichier
[Lien partie1/Vagrantfile-3A](./partie1/Vagrantfile-3A/)

🤖 **Adaptez votre `Vagrantfile`** pour qu'il lance les VMs suivantes (en réutilisant votre box de la partie précédente)

- l'idéal c'est de déclarer une liste en début de fichier qui contient les données des VMs et de faire un `for` sur cette liste
- à vous de voir, sans boucle `for` et sans liste, juste trois blocs déclarés, ça fonctionne aussi

| Name           | IP locale    | Accès internet | RAM |
| -------------- | ------------ | -------------- | --- |
| `alice.tp3.b2` | `10.3.1.11`  | Ui             | 1G  |
| `bob.tp3.b2`   | `10.3.1.200` | Ui             | 2G  |
| `eve.tp3.b2`   | `10.3.1.57`  | Nan            | 1G  |

- 📂 fichier
[Lien partie1/Vagrantfile-3B](./partie1/Vagrantfile-3B/)

