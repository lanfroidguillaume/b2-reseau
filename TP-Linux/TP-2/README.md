# TP2 : Utilisation courante de Docker

## Sommaire

- [TP2 : Utilisation courante de Docker](#tp2--utilisation-courante-de-docker)
  - [Sommaire](#sommaire)
- [I. Commun à tous : PHP](#tp2-commun--stack-php)
- [II Admin. Maîtrise de la stack PHP](#tp2-admins--php-stack)

---

# TP2 Commun : Stack PHP

## Sommaire

- [TP2 Commun : Stack PHP](#tp2-commun--stack-php)
  - [Sommaire](#sommaire)
- [I. Packaging de l'app PHP](#i-packaging-de-lapp-php)


# I. Packaging de l'app PHP

🤖 **`docker-compose.yml`**

- 📂 fichier
[Lien vers docker-compose](./PHP/docker-compose.yml)
[Lien vers seed](./PHP/sql/seed.sql)

---

# TP2 admins : PHP stack

## Sommaire

- [TP2 admins : PHP stack](#tp2-admins--php-stack)
  - [Sommaire](#sommaire)
- [I. Good practices](#i-good-practices)
- [II. Reverse proxy buddy](#ii-reverse-proxy-buddy)
  - [A. Simple HTTP setup](#a-simple-http-setup)
  - [B. HTTPS auto-signé](#b-https-auto-signé)
  - [C. HTTPS avec une CA maison](#c-https-avec-une-ca-maison)


# I. Good practices

🤖 **Limiter l'accès aux ressources**

- limiter la RAM que peut utiliser chaque conteneur à 1G
- limiter à 1CPU chaque conteneur
```docker-compose.yml
version: '3'
services:
  service_name:
    image: nom_de_l_image
    deploy:
     sources re:
        limits:
          memory: 1g
          cpus: '1.0'
```

🤖 **No `root`**

- s'assurer que chaque conteneur n'utilise pas l'utilisateur `root`
- mais bien un utilisateur dédié
- on peut préciser avec une option du `run` sous quelle identité le processus sera lancé
```docker-compose.yml
version: '3'
services:
  service_name:
    image: nom_de_l_image
    user: "user:group"
```

# II. Reverse proxy buddy

## A. Simple HTTP setup

🤖 **Adaptez le `docker-compose.yml`** de [la partie précédente](./php.md)

- il doit inclure un quatrième conteneur : un reverse proxy NGINX
- je vous file une conf minimale juste en dessous
- c'est le seul conteneur exposé (partage de ports)
- vous ajouterez au fichier `hosts` de **votre PC** (le client)
```nginx
server {
    listen       80;
    server_name  www.supersite.com;

    location / {
        proxy_pass   http://nom_du_conteneur_PHP;
    }
}

server {
    listen       80;
    server_name  pma.supersite.com;

    location / {
        proxy_pass   http://nom_du_conteneur_PMA;
    }
}
```

- 📂 fichier
[Lien vers docker-compose](./Admin/docker-compose.yml)
[Lien vers seed](./Admin/sql/seed.sql)

## B. HTTPS auto-signé

🤖 **HTTPS** auto-signé

- générez un certificat et une clé auto-signés
- adaptez la conf de NGINX pour tout servir en HTTPS
- la clé et le certificat doivent être montés avec des volumes (`-v`)
- la commande pour générer une clé et un cert auto-signés :
```bash
openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -keyout www.supersite.com.key -out www.supersite.com.crt
```
```
cat nginx/nginx.conf
events {

}

http {
    server {
        listen 80;
        server_name www.supersite.com;

        location / {
            return 301 https://$host$request_uri;
        }
    }

    server {
        listen 443 ssl;
        server_name www.supersite.com;

        ssl_certificate /etc/nginx/certs/www.supersite.com.crt;
        ssl_certificate_key /etc/nginx/certs/www.supersite.com.key;

        location / {
            proxy_pass http://php-apache;
        }
    }

    server {
        listen 8080;
        server_name pma.supersite.com;

        return 301 https://$host$request_uri;
    }

    server {
        listen 443 ssl;
        server_name pma.supersite.com;

        ssl_certificate /etc/nginx/certs/pma.supersite.com.crt;
        ssl_certificate_key /etc/nginx/certs/pma.supersite.com.key;

        location / {
            proxy_pass http://phpmyadmin;
        }
    }
}
 ~/Documents/B2_info/linux/B2_TP-Linux/TP2/php cat docker-compose.yml 
 version: '3'

services:
  web:
    image: php:8.0.0-apache
    volumes:
      - ./src/:/var/www/html/
    ports:
      - 8000:80
    deploy:
      resources:
        limits:
          cpus: '1'  
          memory: 1g   
      user: 'robot-045'    

  db:
    image: mysql:8.2.0
    volumes:
      - db_data:/var/lib/mysql
      - ./sql/seed.sql:/docker-entrypoint-initdb.d/seed.sql
    environment:
      MYSQL_ROOT_PASSWORD: root
      MYSQL_DATABASE: db
      MYSQL_USER: robot-045
      MYSQL_PASSWORD: guillaume
    deploy:
      resources:
        limits:
          cpus: '1' 
          memory: 1g   
      user: 'robot-045'     

  phpmyadmin:
    depends_on:
      - db
    image: phpmyadmin/phpmyadmin
    environment:
      PMA_HOST: db
    ports:
      - 2000:80
    deploy:
      resources:
        limits:
          cpus: '1' 
          memory: 1g  
      user: 'robot-045'
        
  nginx:
    image: nginx:latest
    volumes:
      - ./nginx/nginx.conf:/etc/nginx/nginx.conf
    ports:
      - 80:80
    deploy:
      resources:
        limits:
          cpus: '1'  
          memory: 1g   
      user: 'robot-045'     

volumes:
  db_data: 
```
## C. HTTPS avec une CA maison

🤖 **Ajustez la configuration NGINX**

```
server {
    [...]
    # faut changer le listen
    listen 10.7.1.103:443 ssl;

    # et ajouter ces deux lignes
    ssl_certificate /chemin/vers/le/cert/www.supersite.com.crt;
    ssl_certificate_key /chemin/vers/la/clé/www.supersite.com.key;
    [...]
}
```
```
ls certs/
CA.key  CA.srl                 pma.supersite.com.csr  v3.ext                 www.supersite.com.csr
CA.pem  pma.supersite.com.crt  pma.supersite.com.key  www.supersite.com.crt  www.supersite.com.key
```
🤖 **Prouvez avec un `curl` que vous accédez au site web**

- depuis votre PC
- avec un `curl -k` car il ne reconnaît pas le certificat là

🤖 **Ajouter le certificat de la CA dans votre navigateur**

- vous pourrez ensuite visitez `https://web.tp7.b2` sans alerte de sécurité, et avec un cadenas vert
- il est nécessaire de joindre le site avec son nom pour que HTTPS fonctionne (fichier `hosts`)

