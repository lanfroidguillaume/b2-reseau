# TP5 Admin : Haute-Dispo

## Sommaire

- [TP4 : Vers une maîtrise des OS Linux](#tp5-admin--haute-dispo)
  - [Sommaire](#sommaire)
- [I. Parti 1](#partie-1)
- [II. Parti 2](#partie-2)

# Partie 1
- [**Partie 1 : Setup initial**](./Partie_1.md)

# Partie 2
- [**Partie 2 : Haute Disponibilité**](./Partie_2.md)
