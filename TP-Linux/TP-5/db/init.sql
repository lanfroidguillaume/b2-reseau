-- Utilisation de la base de données "app_nulle"
USE app_nulle;

-- Création de la table "meo" si elle n'existe pas déjà
CREATE TABLE IF NOT EXISTS meo
(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);
