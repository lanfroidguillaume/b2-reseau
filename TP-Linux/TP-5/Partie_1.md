# Partie 1 : Setup du lab

## Sommaire

- [Partie 1 : Setup du lab](#partie-1--setup-du-lab)
  - [Sommaire](#sommaire)
  - [0. Setup](#0-setup)
  - [1. Lab initial](#1-lab-initial)
    - [A. Présentation](#a-présentation)
  - [B. L'app web](#b-lapp-web)
  - [C. Monter le lab](#c-monter-le-lab)

## 0. Setup

## 1. Lab initial

### A. Présentation

- **app web**
  - on l'appellera `app_nulle`
  - portée par une VM `web1.tp5.b2`
- **un ptit reverse proxy devant**
  - il sert l'application `app_nulle`
  - porté par une VM `rp1.tp5.b2`
- **une base de données derrière**
  - elle stocke les données de l'application `app_nulle`
  - portée par une VM `db1.tp5.b2`

Un client pourra saisir le nom `http://app_nulle.tp5.b2` pour accéder à l'application.

| Node          | Adresse      | Rôle                       |
| ------------- | ------------ | -------------------------- |
| `web1.tp5.b2` | `10.5.1.11`  | Serveur Web (Apache + PHP) |
| `rp1.tp5.b2`  | `10.5.1.111` | Reverse Proxy (NGINX)      |
| `db1.tp5.b2`  | `10.5.1.211` | DB (MariaDB)               |

## B. L'app web

## C. Monter le lab

🌞 **A rendre**

- le `Vagrantfile`
- les **scripts** qui effectuent la conf
- le README explique juste qu'il faut `vagrant up` et éventuellement taper deux trois commandes après si nécessaire

```bash
vagrant up
```
```bash
ssh vagrant web1.tp5.b2
cd /var/serv
chmod 744 web.ch
sudo ./web.sh
```
```bash
ssh vagrant db1.tp5.b2
cd /var/db
chmod 744 db.sh init.sql 
sudo ./db.sh
```
```bash
ssh vagrant rp1.tp5.b2
cd /var/reverse_proxy
chmod 744 rp.sh 
sudo ./rp.sh
```