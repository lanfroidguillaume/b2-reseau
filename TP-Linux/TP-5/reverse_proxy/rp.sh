#!/bin/bash

# Ajout des entrées dans le fichier /etc/hosts pour la résolution DNS locale
echo "10.5.1.11 web1.tp5.b2" | sudo tee -a /etc/hosts >/dev/null
echo "10.5.1.211 db1.tp5.b2" | sudo tee -a /etc/hosts >/dev/null

# Copie du fichier de configuration Nginx
sudo cp app_nulle.conf /etc/nginx/conf.d/app_nulle.conf

# Installation de Nginx
sudo dnf install nginx -y

# Démarrage et activation du service Nginx
sudo systemctl enable nginx
sudo systemctl start nginx

# Ouverture du port HTTP dans le pare-feu
sudo firewall-cmd --permanent --add-service=http
sudo firewall-cmd --reload
