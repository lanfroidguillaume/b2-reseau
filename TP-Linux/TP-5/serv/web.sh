#!/bin/bash

PROJECT_DIR="/var/serv"
NEW_USER="web"

# Ajout des entrées dans le fichier /etc/hosts pour la résolution DNS locale
echo "10.5.1.111 rp1.tp5.b2" | sudo tee -a /etc/hosts >/dev/null
echo "10.5.1.211 db1.tp5.b2" | sudo tee -a /etc/hosts >/dev/null

# Configuration du pare-feu pour autoriser le trafic sur le port 80
firewall-cmd --add-port=80/tcp --permanent
firewall-cmd --reload

# Installation de Docker
dnf install -y dnf-plugins-core
dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
dnf install -y docker-ce docker-ce-cli containerd.io

# Démarrage et activation du service Docker
systemctl start docker
systemctl enable docker

# Création de l'utilisateur et ajout au groupe Docker
useradd -m $NEW_USER
usermod -aG docker $NEW_USER

# Création du fichier de service systemd
tee /etc/systemd/system/serv.service > /dev/null <<EOF
[Unit]
Description=Serv Start Service
After=network.target

[Service]
User=$NEW_USER
Group=docker
WorkingDirectory=$PROJECT_DIR
Restart=on-failure
ExecStart=/bin/bash ./compose.sh

[Install]
WantedBy=multi-user.target
EOF

# Création du script compose.sh
tee ./compose.sh > /dev/null <<EOF
#!/bin/bash
docker-compose up -d
EOF

# Attribution des droits appropriés au script compose.sh
chown $NEW_USER:$NEW_USER ./compose.sh
chmod +x ./compose.sh

# Rechargement des services systemd
systemctl daemon-reload

# Activation et démarrage du service
systemctl enable serv.service
systemctl start serv.service
