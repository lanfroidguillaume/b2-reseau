# TP8 INFRA : Make ur own

## Sommaire

- [TP8 INFRA : Make ur own](#tp8-infra--make-ur-own)
  - [Sommaire](#sommaire)
  - [I. Rendu attendu](#i-rendu-attendu)
    - [I. Le site 1](#le-site-meow-origins)
      - [1. Topologie réseau](#a-topologie-réseau)
      - [2. Tableau d'adressage](#b-tableau-dadressage)
      - [3. Topologie réseau](#c-tableau-des-vlans)
      - [3. Topologie réseau](#)
    - [II. Le site 2](#le-site-meow-and-beyond)
      - [1. Topologie réseau](#a-topologie-réseau-1)
      - [2. Tableau d'adressage](#b-tableau-dadressage-1)
      - [3. Topologie réseau](#c-tableau-des-vlans-1)

  ## I. Rendu attendu

- 🖼 image ![image Schéma](./image/image-schema.png)

---

conection entre les deux site 

| Machine - Réseau  | `10.3.10.0/30` |
| :---------------: | :------------: |
| `R1`              |   `10.3.10.4`  |
| `R2`              |   `10.3.10.5`  |

## le site "Meow Origins"

### A. Topologie réseau

- 🤖 **Schéma réseau**

- 🖼 image ![image Schéma 1](./image/image-schema-1.png)

🤖 **Tableaux d'adressage et VLAN**

### B. Tableau d'adressage

| Machine - Réseau  | `10.1.20.0/26` | `10.1.30.0/27` | `10.1.40.0/23` | `10.1.50.0/22` | `10.1.60.0/25` | `10.1.70.0/26` | `10.1.80.0/26` |
| :---------------: | :------------: | :------------: | :------------: | :------------: | :------------: | :------------: | :------------: |
| `CAM7`            |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |   `10.1.80.7`  |
| `CAM8`            |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |   `10.1.80.8`  |
| `CAM9`            |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |   `10.1.80.9`  |
| `CAM10`           |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |   `10.1.80.10` |
| `CAM11`           |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |   `10.1.80.11` |
| `CAM12`           |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |   `10.1.80.12` |
| `CAM13`           |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |   `10.1.80.13` |
| `CAM14`           |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |   `10.1.80.14` |
| `CAM15`           |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |   `10.1.80.15` |
| `CAM16`           |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |   `10.1.80.16` |
| `CAM17`           |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |   `10.1.80.17` |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `TV7`             |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |   `10.1.70.7`  |        ❌      |
| `TV8`             |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |   `10.1.70.8`  |        ❌      |
| `TV9`             |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |   `10.1.70.9`  |        ❌      |
| `TV10`            |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |   `10.1.70.10` |        ❌      |
| `TV11`            |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |   `10.1.70.11` |        ❌      |
| `TV12`            |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |   `10.1.70.12` |        ❌      |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `IM3`             |        ❌     |        ❌      |       ❌      |       ❌       |   `10.1.60.3`  |        ❌     |        ❌      |
| `IM4`             |        ❌     |        ❌      |       ❌      |       ❌       |   `10.1.60.4`  |        ❌     |        ❌      |
| `IM5`             |        ❌     |        ❌      |       ❌      |       ❌       |   `10.1.60.5`  |        ❌     |        ❌      |
| `IM6`             |        ❌     |        ❌      |       ❌      |       ❌       |   `10.1.60.6`  |        ❌     |        ❌      |
| `IM7`             |        ❌     |        ❌      |       ❌      |       ❌       |   `10.1.60.7`  |        ❌     |        ❌      |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `PDG1`            |  `10.1.20.03`  |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `RH1`             |  `10.1.20.11`  |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `RH2`             |  `10.1.20.12`  |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `SE3`             |  `10.1.20.13`  |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `SE4`             |  `10.1.20.14`  |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `SE5`             |  `10.1.20.15`  |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `SE6`             |  `10.1.20.16`  |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `SE7`             |  `10.1.20.17`  |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `plat-tests-1`    |  `10.1.20.20`  |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `git`             |  `10.1.20.21`  |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `plat-produc`     |  `10.1.20.22`  |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `ADMIN-reseau2`   |        ❌     |   `10.1.30.5`  |        ❌      |       ❌       |       ❌      |        ❌      |       ❌      |
| `ADMIN-sys2`      |        ❌     |   `10.1.30.3`  |        ❌      |       ❌       |       ❌      |        ❌      |       ❌      |
| `RESP-sécu1`      |        ❌     |   `10.1.30.4`  |        ❌      |       ❌       |       ❌      |        ❌      |       ❌      |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `dhcp.site1-1`    |        ❌     |        ❌      |  `10.1.40.253` |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV01`           |        ❌     |        ❌      |      DHCP      |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV...`          |        ❌     |        ❌      |      `...`     |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV0139`         |        ❌     |        ❌      |      DHCP      |       ❌       |       ❌      |        ❌      |        ❌     |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `T01`             |        ❌     |        ❌      |       ❌      |      DHCP       |       ❌      |        ❌      |        ❌     |
| `T...`            |        ❌     |        ❌      |       ❌      |      `...`      |       ❌      |        ❌      |        ❌     |
| `T0150`           |        ❌     |        ❌      |       ❌      |      DHCP       |       ❌      |        ❌      |        ❌     |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `R2`              |   `10.1.20.1`  |   `10.1.30.1`  |   `10.1.40.1`  |   `10.1.50.1`  |   `10.1.60.1`  |   `10.1.70.1`  |   `10.1.80.1`  |


### C. Tableau des VLANs

- Association VLAN <> réseau IP

| VLAN                | Réseau IP associé |
| :------------------ | :---------------: |
| VLAN 2 `direction`  | `10.1.20.0/26`    |
| VLAN 3 `admins`     | `10.1.30.0/27`    |
| VLAN 4 `dev`        | `10.1.40.0/23`    |
| VLAN 5 `telephone`  | `10.1.50.0/22`    |
| VLAN 6 `impr`       | `10.1.60.0/25`    |
| VLAN 7 `tele`       | `10.1.70.0/26`    |
| VLAN 8 `cam`        | `10.1.80.0/26`    |

- Quel client est dans quel VLAN

| VLAN              | VLAN 2 `direction` | VLAN 3 `admins` | VLAN 4 `dev`  | VLAN 5 `telephone` | VLAN 6 `impr` | VLAN 7 `tele` | VLAN 8 `cam`  |
| :---------------: | :----------------: | :-------------: | :-----------: | :--------------:   | :-----------: | :-----------: | :-----------: |
| `CAM7`            |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |       ✅       |
| `CAM8`            |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |       ✅       |
| `CAM9`            |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |       ✅       |
| `CAM10`           |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |       ✅       |
| `CAM11`           |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |       ✅       |
| `CAM12`           |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |       ✅       |
| `CAM13`           |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |       ✅       |
| `CAM14`           |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |       ✅       |
| `CAM15`           |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |       ✅       |
| `CAM16`           |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |       ✅       |
| `CAM17`           |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |       ✅       |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `TV7`             |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |       ✅       |        ❌      |
| `TV8`             |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |       ✅       |        ❌      |
| `TV9`             |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |       ✅       |        ❌      |
| `TV10`            |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |       ✅       |        ❌      |
| `TV11`            |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |       ✅       |        ❌      |
| `TV12`            |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |       ✅       |        ❌      |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `IM3`             |        ❌     |        ❌      |       ❌      |       ❌       |       ✅       |        ❌     |        ❌      |
| `IM4`             |        ❌     |        ❌      |       ❌      |       ❌       |       ✅       |        ❌     |        ❌      |
| `IM5`             |        ❌     |        ❌      |       ❌      |       ❌       |       ✅       |        ❌     |        ❌      |
| `IM6`             |        ❌     |        ❌      |       ❌      |       ❌       |       ✅       |        ❌     |        ❌      |
| `IM7`             |        ❌     |        ❌      |       ❌      |       ❌       |       ✅       |        ❌     |        ❌      |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `PDG1`            |       ✅       |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `RH1`             |       ✅       |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `RH2`             |       ✅       |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `SE1`             |       ✅       |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `SE2`             |       ✅       |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `SE3`             |       ✅       |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `SE4`             |       ✅       |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `SE5`             |       ✅       |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `plat-tests-1`    |       ✅       |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `git`             |       ✅       |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `plat-produc`     |       ✅       |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `ADMIN-reseau2`   |        ❌     |       ✅       |        ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `ADMIN-sys2`      |        ❌     |       ✅       |        ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `RESP-sécu1`      |        ❌     |       ✅       |        ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `dhcp.site1-1`    |        ❌     |        ❌      |       ✅       |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV1`            |        ❌     |        ❌      |       ✅       |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV...`          |        ❌     |        ❌      |       ✅       |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV139`          |        ❌     |        ❌      |       ✅       |       ❌       |       ❌      |        ❌      |        ❌     |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `T1`              |        ❌     |        ❌      |       ❌      |       ✅       |       ❌      |        ❌      |        ❌     |
| `T...`            |        ❌     |        ❌      |       ❌      |       ✅       |       ❌      |        ❌      |        ❌     |
| `T150`            |        ❌     |        ❌      |       ❌      |       ✅       |       ❌      |        ❌      |        ❌     |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `R2`              |       ✅      |      ✅       |      ✅       |      ✅       |      ✅       |      ✅       |      ✅       |

### D. conf

🤖 **Config de toutes les machines**

- **routeur**
```
R2#sh run
Building configuration...

Current configuration : 2519 bytes
!
version 12.4
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
!
hostname R2
!
boot-start-marker
boot-end-marker
!
!
no aaa new-model
memory-size iomem 5
no ip icmp rate-limit unreachable
ip cef
!
!
no ip domain lookup
!
multilink bundle-name authenticated
!
!
archive
 log config
  hidekeys
!
!
ip tcp synwait-time 5
!
!
interface FastEthernet0/0
 ip address dhcp
 ip nat outside
 ip virtual-reassembly
 duplex auto
 speed auto
!
interface FastEthernet0/1
 ip address 10.3.10.5 255.255.255.252
 duplex auto
 speed auto
!
interface FastEthernet1/0
 no ip address
 ip nat inside
 ip virtual-reassembly
 shutdown
 duplex auto
 speed auto
!
interface FastEthernet1/0.2
 encapsulation dot1Q 2
 ip address 10.1.20.1 255.255.255.192
!
interface FastEthernet1/0.3
 encapsulation dot1Q 3
 ip address 10.1.30.1 255.255.255.224
!
interface FastEthernet1/0.4
 encapsulation dot1Q 4
 ip address 10.1.40.1 255.255.254.0
!
interface FastEthernet1/0.5
 encapsulation dot1Q 5
 ip address 10.1.50.1 255.255.252.0
!
interface FastEthernet1/0.6
 encapsulation dot1Q 6
 ip address 10.1.60.1 255.255.255.128
!
interface FastEthernet1/0.7
 encapsulation dot1Q 7
 ip address 10.1.70.1 255.255.255.192
!
interface FastEthernet1/0.8
 encapsulation dot1Q 8
 ip address 10.1.80.1 255.255.255.192
!
interface FastEthernet2/0
 no ip address
 ip nat inside
 ip virtual-reassembly
 shutdown
 duplex auto
 speed auto
!
interface FastEthernet2/0.2
 encapsulation dot1Q 2
 ip address 10.1.20.2 255.255.255.192
!
interface FastEthernet2/0.3
 encapsulation dot1Q 3
 ip address 10.1.30.2 255.255.255.224
!
interface FastEthernet2/0.4
 encapsulation dot1Q 4
 ip address 10.1.40.2 255.255.254.0
!
interface FastEthernet2/0.5
 encapsulation dot1Q 5
 ip address 10.1.50.2 255.255.252.0
!
interface FastEthernet2/0.6
 encapsulation dot1Q 6
 ip address 10.1.60.2 255.255.255.128
!
interface FastEthernet2/0.7
 encapsulation dot1Q 7
 ip address 10.1.70.2 255.255.255.192
!
interface FastEthernet2/0.8
 encapsulation dot1Q 8
 ip address 10.1.80.2 255.255.255.192
!
ip forward-protocol nd
!
!
no ip http server
no ip http secure-server
ip nat inside source list 1 interface FastEthernet0/0 overload
!
access-list 1 permit any
no cdp log mismatch duplex
!
!
control-plane
!
!
line con 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line aux 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line vty 0 4
 login
!
!
end
```

- **switches**

- core
 `sw1` `sw2`
```
sw2#sh run
Building configuration...

Current configuration : 1865 bytes
!
! Last configuration change at 13:46:25 UTC Thu Dec 21 2023
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname sw2
!
boot-start-marker
boot-end-marker
!
!
logging discriminator EXCESS severity drops 6 msg-body drops EXCESSCOLL
logging buffered 50000
logging console discriminator EXCESS
!
no aaa new-model
!
!
no ip icmp rate-limit unreachable
!
!
no ip domain-lookup
ip cef
no ipv6 cef
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
!
interface Port-channel1
!
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/1
 channel-group 1 mode on
!
interface Ethernet0/2
 channel-group 1 mode on
!
interface Ethernet0/3
!
interface Ethernet1/0
!
interface Ethernet1/1
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet1/2
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet1/3
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet2/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet2/1
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet2/2
!
interface Ethernet2/3
!
interface Ethernet3/0
!
interface Ethernet3/1
!
interface Ethernet3/2
!
interface Ethernet3/3
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan2
 no ip address
!
interface Vlan3
 no ip address
!
interface Vlan4
 no ip address
!
interface Vlan5
 no ip address
!
interface Vlan6
 no ip address
!
interface Vlan7
 no ip address
!
interface Vlan8
 no ip address
!
ip forward-protocol nd
!
ip tcp synwait-time 5
ip http server
!
ip ssh server algorithm encryption aes128-ctr aes192-ctr aes256-ctr
ip ssh client algorithm encryption aes128-ctr aes192-ctr aes256-ctr
!
!
control-plane
!
!
line con 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line aux 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line vty 0 4
 login
!
!
end
```
```
sw1#sh run
Building configuration...

Current configuration : 1865 bytes
!
! Last configuration change at 13:46:35 UTC Thu Dec 21 2023
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname sw1
!
boot-start-marker
boot-end-marker
!
!
logging discriminator EXCESS severity drops 6 msg-body drops EXCESSCOLL
logging buffered 50000
logging console discriminator EXCESS
!
no aaa new-model
!
!
no ip icmp rate-limit unreachable
!
!
no ip domain-lookup
ip cef
no ipv6 cef
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
!
interface Port-channel1
!
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/1
 channel-group 1 mode on
!
interface Ethernet0/2
 channel-group 1 mode on
!
interface Ethernet0/3
!
interface Ethernet1/0
!
interface Ethernet1/1
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet1/2
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet1/3
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet2/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet2/1
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet2/2
!
interface Ethernet2/3
!
interface Ethernet3/0
!
interface Ethernet3/1
!
interface Ethernet3/2
!
interface Ethernet3/3
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan2
 no ip address
!
interface Vlan3
 no ip address
!
interface Vlan4
 no ip address
!
interface Vlan5
 no ip address
!
interface Vlan6
 no ip address
!
interface Vlan7
 no ip address
!
interface Vlan8
 no ip address
!
ip forward-protocol nd
!
ip tcp synwait-time 5
ip http server
!
ip ssh server algorithm encryption aes128-ctr aes192-ctr aes256-ctr
ip ssh client algorithm encryption aes128-ctr aes192-ctr aes256-ctr
!
!
control-plane
!
!
line con 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line aux 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line vty 0 4
 login
!
!
end
```

- Distribution, **elle sont tous identique**
  `sw3` `sw4` `sw5` `sw6` `sw7` 
```
sw3#sh run
Building configuration...

Current configuration : 1771 bytes
!
! Last configuration change at 13:46:45 UTC Thu Dec 21 2023
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname sw3
!
boot-start-marker
boot-end-marker
!
!
logging discriminator EXCESS severity drops 6 msg-body drops EXCESSCOLL
logging buffered 50000
logging console discriminator EXCESS
!
no aaa new-model
!
!
no ip icmp rate-limit unreachable
!
!
no ip domain-lookup
ip cef
no ipv6 cef
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
!
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/1
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/2
!
interface Ethernet0/3
!
interface Ethernet1/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet1/1
 switchport access vlan 4
 switchport mode access
!
interface Ethernet1/2
 switchport access vlan 5
 switchport mode access
!
interface Ethernet1/3
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan2
 no ip address
!
interface Vlan3
 no ip address
!
interface Vlan4
 no ip address
!
interface Vlan5
 no ip address
!
interface Vlan6
 no ip address
!
interface Vlan7
 no ip address
!
interface Vlan8
 no ip address
!
ip forward-protocol nd
!
ip tcp synwait-time 5
ip http server
!
ip ssh server algorithm encryption aes128-ctr aes192-ctr aes256-ctr
ip ssh client algorithm encryption aes128-ctr aes192-ctr aes256-ctr
!
!
control-plane
!
!
line con 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line aux 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line vty 0 4
 login
!
!
end
```

- Accés 
  - se qui sont **identique** sont : `IOU13` `IOU15` `IOU17` | `IOU14` `IOU16` `IOU18`
  (les paire avec les paire et les impaire avec les impaire)
```
IOU18#sh run
Building configuration...

Current configuration : 2267 bytes
!
! Last configuration change at 13:55:35 UTC Thu Dec 21 2023
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname IOU18
!
boot-start-marker
boot-end-marker
!
!
logging discriminator EXCESS severity drops 6 msg-body drops EXCESSCOLL
logging buffered 50000
logging console discriminator EXCESS
!
no aaa new-model
!
!
no ip icmp rate-limit unreachable
!
!
no ip domain-lookup
ip cef
no ipv6 cef
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
!
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/1
 switchport access vlan 4
 switchport mode access
!
interface Ethernet0/2
 switchport access vlan 4
 switchport mode access
!
interface Ethernet0/3
 switchport access vlan 4
 switchport mode access
!
interface Ethernet1/0
 switchport access vlan 4
 switchport mode access
!
interface Ethernet1/1
 switchport access vlan 4
 switchport mode access
!
interface Ethernet1/2
 switchport access vlan 4
 switchport mode access
!
interface Ethernet1/3
 switchport access vlan 4
 switchport mode access
!
interface Ethernet2/0
 switchport access vlan 4
 switchport mode access
!
interface Ethernet2/1
 switchport access vlan 4
 switchport mode access
!
interface Ethernet2/2
 switchport access vlan 4
 switchport mode access
!
interface Ethernet2/3
 switchport access vlan 4
 switchport mode access
!
interface Ethernet3/0
 switchport access vlan 4
 switchport mode access
!
interface Ethernet3/1
 switchport access vlan 4
 switchport mode access
!
interface Ethernet3/2
 switchport access vlan 4
 switchport mode access
!
interface Ethernet3/3
 switchport access vlan 4
 switchport mode access
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan4
 no ip address
!
ip forward-protocol nd
!
ip tcp synwait-time 5
ip http server
!
ip ssh server algorithm encryption aes128-ctr aes192-ctr aes256-ctr
ip ssh client algorithm encryption aes128-ctr aes192-ctr aes256-ctr
!
!
control-plane
!
!
line con 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line aux 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line vty 0 4
 login
!
!
end
```
```
IOU17#sh run
Building configuration...

Current configuration : 2267 bytes
!
! Last configuration change at 13:56:42 UTC Thu Dec 21 2023
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname IOU17
!
boot-start-marker
boot-end-marker
!
!
logging discriminator EXCESS severity drops 6 msg-body drops EXCESSCOLL
logging buffered 50000
logging console discriminator EXCESS
!
no aaa new-model
!
!
no ip icmp rate-limit unreachable
!
!
no ip domain-lookup
ip cef
no ipv6 cef
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
!
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/1
 switchport access vlan 5
 switchport mode access
!
interface Ethernet0/2
 switchport access vlan 5
 switchport mode access
!
interface Ethernet0/3
 switchport access vlan 5
 switchport mode access
!
interface Ethernet1/0
 switchport access vlan 5
 switchport mode access
!
interface Ethernet1/1
 switchport access vlan 5
 switchport mode access
!
interface Ethernet1/2
 switchport access vlan 5
 switchport mode access
!
interface Ethernet1/3
 switchport access vlan 5
 switchport mode access
!
interface Ethernet2/0
 switchport access vlan 5
 switchport mode access
!
interface Ethernet2/1
 switchport access vlan 5
 switchport mode access
!
interface Ethernet2/2
 switchport access vlan 5
 switchport mode access
!
interface Ethernet2/3
 switchport access vlan 5
 switchport mode access
!
interface Ethernet3/0
 switchport access vlan 5
 switchport mode access
!
interface Ethernet3/1
 switchport access vlan 5
 switchport mode access
!
interface Ethernet3/2
 switchport access vlan 5
 switchport mode access
!
interface Ethernet3/3
 switchport access vlan 5
 switchport mode access
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan5
 no ip address
!
ip forward-protocol nd
!
ip tcp synwait-time 5
ip http server
!
ip ssh server algorithm encryption aes128-ctr aes192-ctr aes256-ctr
ip ssh client algorithm encryption aes128-ctr aes192-ctr aes256-ctr
!
!
control-plane
!
!
line con 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line aux 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line vty 0 4
 login
!
!
end
```

```
IOU8#sh run
Building configuration...

Current configuration : 1530 bytes
!
! Last configuration change at 14:41:56 UTC Thu Dec 21 2023
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname IOU8
!
boot-start-marker
boot-end-marker
!
!
logging discriminator EXCESS severity drops 6 msg-body drops EXCESSCOLL
logging buffered 50000
logging console discriminator EXCESS
!
no aaa new-model
!
!
no ip icmp rate-limit unreachable
!
!
no ip domain-lookup
ip cef
no ipv6 cef
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
!
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/1
!
interface Ethernet0/2
!
interface Ethernet0/3
!
interface Ethernet1/0
 switchport access vlan 6
 switchport mode access
!
interface Ethernet1/1
 switchport access vlan 7
 switchport mode access
!
interface Ethernet1/2
 switchport access vlan 8
 switchport mode access
!
interface Ethernet1/3
 switchport access vlan 8
 switchport mode access
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan6
 no ip address
!
interface Vlan7
 no ip address
!
interface Vlan8
 no ip address
!
ip forward-protocol nd
!
ip tcp synwait-time 5
ip http server
!
ip ssh server algorithm encryption aes128-ctr aes192-ctr aes256-ctr
ip ssh client algorithm encryption aes128-ctr aes192-ctr aes256-ctr
!
!
control-plane
!
!
line con 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line aux 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line vty 0 4
 login
!
!
end
```
```
IOU9#sh run
Building configuration...

Current configuration : 2088 bytes
!
! Last configuration change at 14:41:53 UTC Thu Dec 21 2023
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname IOU9
!
boot-start-marker
boot-end-marker
!
!
logging discriminator EXCESS severity drops 6 msg-body drops EXCESSCOLL
logging buffered 50000
logging console discriminator EXCESS
!
no aaa new-model
!
!
no ip icmp rate-limit unreachable
!
!
no ip domain-lookup
ip cef
no ipv6 cef
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
!
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/1
!
interface Ethernet0/2
!
interface Ethernet0/3
!
interface Ethernet1/0
 switchport access vlan 6
 switchport mode access
!
interface Ethernet1/1
 switchport access vlan 7
 switchport mode access
!
interface Ethernet1/2
 switchport access vlan 8
 switchport mode access
!
interface Ethernet1/3
 switchport access vlan 8
 switchport mode access
!
interface Ethernet2/0
 switchport access vlan 2
 switchport mode access
!
interface Ethernet2/1
 switchport access vlan 2
 switchport mode access
!
interface Ethernet2/2
 switchport access vlan 2
 switchport mode access
!
interface Ethernet2/3
!
interface Ethernet3/0
 switchport access vlan 3
 switchport mode access
!
interface Ethernet3/1
 switchport access vlan 3
 switchport mode access
!
interface Ethernet3/2
 switchport access vlan 3
 switchport mode access
!
interface Ethernet3/3
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan2
 no ip address
!
interface Vlan3
 no ip address
!
interface Vlan6
 no ip address
!
interface Vlan7
 no ip address
!
interface Vlan8
 no ip address
!
ip forward-protocol nd
!
ip tcp synwait-time 5
ip http server
!
ip ssh server algorithm encryption aes128-ctr aes192-ctr aes256-ctr
ip ssh client algorithm encryption aes128-ctr aes192-ctr aes256-ctr
!
!
control-plane
!
!
line con 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line aux 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line vty 0 4
 login
!
!
end
```
```
IOU10#sh run
Building configuration...

Current configuration : 1856 bytes
!
! Last configuration change at 14:41:48 UTC Thu Dec 21 2023
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname IOU10
!
boot-start-marker
boot-end-marker
!
!
logging discriminator EXCESS severity drops 6 msg-body drops EXCESSCOLL
logging buffered 50000
logging console discriminator EXCESS
!
no aaa new-model
!
!
no ip icmp rate-limit unreachable
!
!
no ip domain-lookup
ip cef
no ipv6 cef
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
!
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/1
!
interface Ethernet0/2
!
interface Ethernet0/3
!
interface Ethernet1/0
 switchport access vlan 6
 switchport mode access
!
interface Ethernet1/1
 switchport access vlan 7
 switchport mode access
!
interface Ethernet1/2
 switchport access vlan 8
 switchport mode access
!
interface Ethernet1/3
 switchport access vlan 8
 switchport mode access
!
interface Ethernet2/0
 switchport access vlan 2
 switchport mode access
!
interface Ethernet2/1
 switchport access vlan 2
 switchport mode access
!
interface Ethernet2/2
!
interface Ethernet2/3
!
interface Ethernet3/0
!
interface Ethernet3/1
!
interface Ethernet3/2
!
interface Ethernet3/3
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan2
 no ip address
!
interface Vlan6
 no ip address
!
interface Vlan7
 no ip address
!
interface Vlan8
 no ip address
!
ip forward-protocol nd
!
ip tcp synwait-time 5
ip http server
!
ip ssh server algorithm encryption aes128-ctr aes192-ctr aes256-ctr
ip ssh client algorithm encryption aes128-ctr aes192-ctr aes256-ctr
!
!
control-plane
!
!
line con 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line aux 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line vty 0 4
 login
!
!
end
```
```
IOU11#sh run
Building configuration...

Current configuration : 1956 bytes
!
! Last configuration change at 14:38:00 UTC Thu Dec 21 2023
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname IOU11
!
boot-start-marker
boot-end-marker
!
!
logging discriminator EXCESS severity drops 6 msg-body drops EXCESSCOLL
logging buffered 50000
logging console discriminator EXCESS
!
no aaa new-model
!
!
no ip icmp rate-limit unreachable
!
!
no ip domain-lookup
ip cef
no ipv6 cef
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
!
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/1
!
interface Ethernet0/2
 switchport access vlan 6
 switchport mode access
!
interface Ethernet0/3
 switchport access vlan 7
 switchport mode access
!
interface Ethernet1/0
 switchport access vlan 7
 switchport mode access
!
interface Ethernet1/1
 switchport access vlan 8
 switchport mode access
!
interface Ethernet1/2
 switchport access vlan 8
 switchport mode access
!
interface Ethernet1/3
 switchport access vlan 8
 switchport mode access
!
interface Ethernet2/0
 switchport access vlan 2
 switchport mode access
!
interface Ethernet2/1
 switchport access vlan 2
 switchport mode access
!
interface Ethernet2/2
!
interface Ethernet2/3
!
interface Ethernet3/0
!
interface Ethernet3/1
!
interface Ethernet3/2
!
interface Ethernet3/3
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan2
 no ip address
!
interface Vlan6
 no ip address
!
interface Vlan7
 no ip address
!
interface Vlan8
 no ip address
!
ip forward-protocol nd
!
ip tcp synwait-time 5
ip http server
!
ip ssh server algorithm encryption aes128-ctr aes192-ctr aes256-ctr
ip ssh client algorithm encryption aes128-ctr aes192-ctr aes256-ctr
!
!
control-plane
!
!
line con 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line aux 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line vty 0 4
 login
!
!
end
```
```
IOU12#sh run
Building configuration...

Current configuration : 1906 bytes
!
! Last configuration change at 14:46:12 UTC Thu Dec 21 2023
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname IOU12
!
boot-start-marker
boot-end-marker
!
!
logging discriminator EXCESS severity drops 6 msg-body drops EXCESSCOLL
logging buffered 50000
logging console discriminator EXCESS
!
no aaa new-model
!
!
no ip icmp rate-limit unreachable
!
!
no ip domain-lookup
ip cef
no ipv6 cef
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
!
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/1
!
interface Ethernet0/2
!
interface Ethernet0/3
!
interface Ethernet1/0
 switchport access vlan 6
 switchport mode access
!
interface Ethernet1/1
 switchport access vlan 7
 switchport mode access
!
interface Ethernet1/2
 switchport access vlan 8
 switchport mode access
!
interface Ethernet1/3
 switchport access vlan 8
 switchport mode access
!
interface Ethernet2/0
!
interface Ethernet2/1
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet2/2
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet2/3
!
interface Ethernet3/0
!
interface Ethernet3/1
 switchport access vlan 2
 switchport mode access
!
interface Ethernet3/2
 switchport access vlan 2
 switchport mode access
!
interface Ethernet3/3
 switchport access vlan 2
 switchport mode access
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan2
 no ip address
!
interface Vlan6
 no ip address
!
interface Vlan7
 no ip address
!
interface Vlan8
 no ip address
!
ip forward-protocol nd
!
ip tcp synwait-time 5
ip http server
!
ip ssh server algorithm encryption aes128-ctr aes192-ctr aes256-ctr
ip ssh client algorithm encryption aes128-ctr aes192-ctr aes256-ctr
!
!
control-plane
!
!
line con 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line aux 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line vty 0 4
 login
!
!
end
```

- **DHCP**

```
[robot-045@dhcp ~]$sudo dnf install -y dhcp
```
```
[robot-045@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
# create new

default-lease-time 600;
max-lease-time 7200;
authoritative;

subnet 10.1.40.0 netmask 255.255.255.0 {
  range 10.1.40.10 10.1.40.200;
  option routers 10.1.40.1;
  option domain-name-servers 1.1.1.1;
}

subnet 10.1.50.0 netmask 255.255.254.0 {
  range 10.1.50.10 10.1.50.250;
  option routers 10.1.50.1;
  option domain-name-servers 1.1.1.1;
}
EOF;
```

- **DNS**

```
[robot-045@dns ~]$sudo dnf install bind bind-utils
```
```
[robot-045@dns ~]$sudo cat /etc/named.conf
# create new

options {
    listen-on port 53 { any; };
    directory   "/var/named";
    dump-file   "/var/named/data/cache_dump.db";
    statistics-file "/var/named/data/named_stats.txt";
    memstatistics-file "/var/named/data/named_mem_stats.txt";
    allow-query     { any; };
    recursion yes;
};

zone "." IN {
    type hint;
    file "named.ca";
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";
```

---
##  le site "Meow and Beyond"

### A. Topologie réseau 

- 🤖 **Schéma réseau**

- 🖼 image ![image Schéma 2](./image/image-schema-2.png)

🤖 **Tableaux d'adressage et VLAN**

### B. Tableau d'adressage

| Machine - Réseau  | `10.2.20.0/27` | `10.2.30.0/27` | `10.2.40.0/24` | `10.2.50.0/23` | `10.2.60.0/27` | `10.2.70.0/27` | `10.2.80.0/26` |
| :---------------: | :------------: | :------------: | :------------: | :------------: | :------------: | :------------: | :------------: |
| `CAM1`            |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |   `10.2.80.2`  |
| `CAM2`            |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |   `10.2.80.3`  |
| `CAM3`            |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |   `10.2.80.4`  |
| `CAM4`            |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |   `10.2.80.5`  |
| `CAM5`            |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |   `10.2.80.6`  |
| `CAM6`            |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |   `10.2.80.7`  |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `TV1`             |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |   `10.2.70.2`  |        ❌      |
| `TV2`             |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |   `10.2.70.3`  |        ❌      |
| `TV3`             |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |   `10.2.70.4`  |        ❌      |
| `TV4`             |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |   `10.2.70.5`  |        ❌      |
| `TV5`             |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |   `10.2.70.6`  |        ❌      |
| `TV6`             |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |   `10.2.70.7`  |        ❌      |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `IM1`             |        ❌     |        ❌      |       ❌      |       ❌       |   `10.2.60.2`  |        ❌     |        ❌      |
| `IM2`             |        ❌     |        ❌      |       ❌      |       ❌       |   `10.2.60.3`  |        ❌     |        ❌      |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `SE1`             |  `10.2.20.21`  |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `SE2`             |  `10.2.20.22`  |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `plat-tests`      |  `10.2.20.11`  |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `ADMIN-reseau1`   |        ❌     |   `10.2.30.2`  |        ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `ADMIN-sys1`      |        ❌     |   `10.2.30.3`  |        ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `dhcp.site2-1`    |        ❌     |        ❌      |  `10.1.40.253` |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV1`            |        ❌     |        ❌      |      DHCP      |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV2`            |        ❌     |        ❌      |      DHCP      |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV3`            |        ❌     |        ❌      |      DHCP      |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV4`            |        ❌     |        ❌      |      DHCP      |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV5`            |        ❌     |        ❌      |      DHCP      |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV6`            |        ❌     |        ❌      |      DHCP      |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV7`            |        ❌     |        ❌      |      DHCP      |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV8`            |        ❌     |        ❌      |      DHCP      |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV9`            |        ❌     |        ❌      |      DHCP      |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV10`           |        ❌     |        ❌      |      DHCP      |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV11`           |        ❌     |        ❌      |      DHCP      |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV12`           |        ❌     |        ❌      |      DHCP      |       ❌       |       ❌      |        ❌      |        ❌     |
| `LEAD-DEV1`       |        ❌     |        ❌      |      DHCP      |       ❌       |       ❌      |        ❌      |        ❌     |
| `LEAD-DEV2`       |        ❌     |        ❌      |      DHCP      |       ❌       |       ❌      |        ❌      |        ❌     |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `TA1`             |        ❌     |        ❌      |       ❌       |      DHCP      |       ❌      |        ❌      |        ❌     |
| `TA2`             |        ❌     |        ❌      |       ❌       |      DHCP      |       ❌      |        ❌      |        ❌     |
| `TS1`             |        ❌     |        ❌      |       ❌       |      DHCP      |       ❌      |        ❌      |        ❌     |
| `TS2`             |        ❌     |        ❌      |       ❌       |      DHCP      |       ❌      |        ❌      |        ❌     |
| `T1`              |        ❌     |        ❌      |       ❌       |      DHCP      |       ❌      |        ❌      |        ❌     |
| `T2`              |        ❌     |        ❌      |       ❌       |      DHCP      |       ❌      |        ❌      |        ❌     |
| `T3`              |        ❌     |        ❌      |       ❌       |      DHCP      |       ❌      |        ❌      |        ❌     |
| `T4`              |        ❌     |        ❌      |       ❌       |      DHCP      |       ❌      |        ❌      |        ❌     |
| `T5`              |        ❌     |        ❌      |       ❌       |      DHCP      |       ❌      |        ❌      |        ❌     |
| `T6`              |        ❌     |        ❌      |       ❌       |      DHCP      |       ❌      |        ❌      |        ❌     |
| `T7`              |        ❌     |        ❌      |       ❌       |      DHCP      |       ❌      |        ❌      |        ❌     |
| `T8`              |        ❌     |        ❌      |       ❌       |      DHCP      |       ❌      |        ❌      |        ❌     |
| `T9`              |        ❌     |        ❌      |       ❌       |      DHCP      |       ❌      |        ❌      |        ❌     |
| `T10`             |        ❌     |        ❌      |       ❌       |      DHCP      |       ❌      |        ❌      |        ❌     |
| `T11`             |        ❌     |        ❌      |       ❌       |      DHCP      |       ❌      |        ❌      |        ❌     |
| `T12`             |        ❌     |        ❌      |       ❌       |      DHCP      |       ❌      |        ❌      |        ❌     |
| `T13`             |        ❌     |        ❌      |       ❌       |      DHCP      |       ❌      |        ❌      |        ❌     |
| `T14`             |        ❌     |        ❌      |       ❌       |      DHCP      |       ❌      |        ❌      |        ❌     |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `R1`              |  `10.2.20.1`   |  `10.2.30.1`   |  `10.2.40.1`   |  `10.2.50.1`   |  `10.2.60.1`   |  `10.2.70.1`   |  `10.2.80.1`   |

### C. Tableau des VLANs

- Association VLAN <> réseau IP

| VLAN                | Réseau IP associé |
| :------------------ | :---------------: |
| VLAN 2 `direction`  | `10.2.20.0/27`    |
| VLAN 3 `admins`     | `10.2.30.0/27`    |
| VLAN 4 `dev`        | `10.2.40.0/24`    |
| VLAN 5 `telephone`  | `10.2.50.0/23`    |
| VLAN 6 `impr`       | `10.2.60.0/27`    |
| VLAN 7 `tele`       | `10.2.70.0/27`    |
| VLAN 8 `cam`        | `10.2.80.0/26`    |

- Quel client est dans quel VLAN

| VLAN              | VLAN 2 `direction` | VLAN 3 `admins` | VLAN 4 `dev`  | VLAN 5 `telephone` | VLAN 6 `impr` | VLAN 7 `tele` | VLAN 8 `cam`  |
| :---------------: | :----------------: | :-------------: | :-----------: | :--------------:   | :-----------: | :-----------: | :-----------: |
| `CAM1`            |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |       ✅       |
| `CAM2`            |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |       ✅       |
| `CAM3`            |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |       ✅       |
| `CAM4`            |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |       ✅       |
| `CAM5`            |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |       ✅       |
| `CAM6`            |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |       ✅       |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `TV1`             |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |       ✅       |        ❌      |
| `TV2`             |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |       ✅       |        ❌      |
| `TV3`             |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |       ✅       |        ❌      |
| `TV4`             |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |       ✅       |        ❌      |
| `TV5`             |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |       ✅       |        ❌      |
| `TV6`             |        ❌     |        ❌      |       ❌      |       ❌       |       ❌      |       ✅       |        ❌      |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `IM1`             |        ❌     |        ❌      |       ❌      |       ❌       |       ✅       |        ❌     |        ❌      |
| `IM2`             |        ❌     |        ❌      |       ❌      |       ❌       |       ✅       |        ❌     |        ❌      |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `SE1`             |       ✅       |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `SE2`             |       ✅       |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `plat-tests`      |       ✅       |        ❌      |       ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `ADMIN-reseau1`   |        ❌     |       ✅       |        ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| `ADMIN-sys1`      |        ❌     |       ✅       |        ❌      |       ❌       |       ❌      |        ❌      |        ❌     |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `dhcp.site2-1`    |        ❌     |        ❌      |       ✅       |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV1`            |        ❌     |        ❌      |       ✅       |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV2`            |        ❌     |        ❌      |       ✅       |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV3`            |        ❌     |        ❌      |       ✅       |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV4`            |        ❌     |        ❌      |       ✅       |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV5`            |        ❌     |        ❌      |       ✅       |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV6`            |        ❌     |        ❌      |       ✅       |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV7`            |        ❌     |        ❌      |       ✅       |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV8`            |        ❌     |        ❌      |       ✅       |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV9`            |        ❌     |        ❌      |       ✅       |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV10`           |        ❌     |        ❌      |       ✅       |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV11`           |        ❌     |        ❌      |       ✅       |       ❌       |       ❌      |        ❌      |        ❌     |
| `DEV12`           |        ❌     |        ❌      |       ✅       |       ❌       |       ❌      |        ❌      |        ❌     |
| `LEAD-DEV1`       |        ❌     |        ❌      |       ✅       |       ❌       |       ❌      |        ❌      |        ❌     |
| `LEAD-DEV2`       |        ❌     |        ❌      |       ✅       |       ❌       |       ❌      |        ❌      |        ❌     |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `TA1`             |        ❌     |        ❌      |       ❌       |       ✅       |       ❌      |        ❌      |        ❌     |
| `TA2`             |        ❌     |        ❌      |       ❌       |       ✅       |       ❌      |        ❌      |        ❌     |
| `TS1`             |        ❌     |        ❌      |       ❌       |       ✅       |       ❌      |        ❌      |        ❌     |
| `TS2`             |        ❌     |        ❌      |       ❌       |       ✅       |       ❌      |        ❌      |        ❌     |
| `T1`              |        ❌     |        ❌      |       ❌       |       ✅       |       ❌      |        ❌      |        ❌     |
| `T2`              |        ❌     |        ❌      |       ❌       |       ✅       |       ❌      |        ❌      |        ❌     |
| `T3`              |        ❌     |        ❌      |       ❌       |       ✅       |       ❌      |        ❌      |        ❌     |
| `T4`              |        ❌     |        ❌      |       ❌       |       ✅       |       ❌      |        ❌      |        ❌     |
| `T5`              |        ❌     |        ❌      |       ❌       |       ✅       |       ❌      |        ❌      |        ❌     |
| `T6`              |        ❌     |        ❌      |       ❌       |       ✅       |       ❌      |        ❌      |        ❌     |
| `T7`              |        ❌     |        ❌      |       ❌       |       ✅       |       ❌      |        ❌      |        ❌     |
| `T8`              |        ❌     |        ❌      |       ❌       |       ✅       |       ❌      |        ❌      |        ❌     |
| `T9`              |        ❌     |        ❌      |       ❌       |       ✅       |       ❌      |        ❌      |        ❌     |
| `T10`             |        ❌     |        ❌      |       ❌       |       ✅       |       ❌      |        ❌      |        ❌     |
| `T11`             |        ❌     |        ❌      |       ❌       |       ✅       |       ❌      |        ❌      |        ❌     |
| `T12`             |        ❌     |        ❌      |       ❌       |       ✅       |       ❌      |        ❌      |        ❌     |
| **-------------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** | **----------** |
| `R1`              |       ✅      |      ✅       |      ✅       |      ✅       |      ✅       |      ✅       |      ✅       |


### D. conf

🤖 **Config de toutes les machines**

- **routeur**
```
R1#sh run
Building configuration...

Current configuration : 1869 bytes
!
version 12.4
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
!
hostname R1
!
boot-start-marker
boot-end-marker
!
!
no aaa new-model
memory-size iomem 5
no ip icmp rate-limit unreachable
ip cef
!
!
no ip domain lookup
!
multilink bundle-name authenticated
!
!
archive
 log config
  hidekeys
!
!
ip tcp synwait-time 5
!
!
interface FastEthernet0/0
 ip address dhcp
 ip nat outside
 ip virtual-reassembly
 duplex auto
 speed auto
!
interface FastEthernet0/1
 ip address 10.3.10.4 255.255.255.252
 duplex auto
 speed auto
!
interface FastEthernet1/0
 no ip address
 ip nat inside
 ip virtual-reassembly
 duplex auto
 speed auto
!
interface FastEthernet1/0.2
 encapsulation dot1Q 2
 ip address 10.2.20.1 255.255.255.224
!
interface FastEthernet1/0.3
 encapsulation dot1Q 3
 ip address 10.2.30.1 255.255.255.224
!
interface FastEthernet1/0.4
 encapsulation dot1Q 4
 ip address 10.2.40.1 255.255.255.0
 ip helper-address 10.2.40.245
!
interface FastEthernet1/0.5
 encapsulation dot1Q 5
 ip address 10.2.50.1 255.255.254.0
!
interface FastEthernet1/0.6
 encapsulation dot1Q 6
 ip address 10.2.60.1 255.255.255.224
!
interface FastEthernet1/0.7
 encapsulation dot1Q 7
 ip address 10.2.70.1 255.255.255.224
!
interface FastEthernet1/0.8
 encapsulation dot1Q 8
 ip address 10.2.80.1 255.255.255.192
!
interface FastEthernet2/0
 no ip address
 shutdown
 duplex auto
 speed auto
!
ip forward-protocol nd
!
!
no ip http server
no ip http secure-server
ip nat inside source list 1 interface FastEthernet0/0 overload
!
access-list 1 permit any
no cdp log mismatch duplex
!
!
control-plane
!
!
line con 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line aux 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line vty 0 4
 login
!
!
end
```
- **switches**
 - Bâtiment 1
```
IOU1#sh run
Building configuration...

Current configuration : 1497 bytes
!
! Last configuration change at 14:08:18 UTC Sun Dec 17 2023
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname IOU1
!
boot-start-marker
boot-end-marker
!
!
logging discriminator EXCESS severity drops 6 msg-body drops EXCESSCOLL
logging buffered 50000
logging console discriminator EXCESS
!
no aaa new-model
!
!
no ip icmp rate-limit unreachable
!
!
no ip domain-lookup
ip cef
no ipv6 cef
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
!
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/1
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/2
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/3
 switchport access vlan 5
 switchport mode access
!
interface Ethernet1/0
!
interface Ethernet1/1
!
interface Ethernet1/2
!
interface Ethernet1/3
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan2
 no ip address
!
interface Vlan3
 no ip address
!
interface Vlan4
 no ip address
!
interface Vlan5
 no ip address
!
interface Vlan6
 no ip address
!
interface Vlan7
 no ip address
!
interface Vlan8
 no ip address
!
ip forward-protocol nd
!
ip tcp synwait-time 5
ip http server
!
ip ssh server algorithm encryption aes128-ctr aes192-ctr aes256-ctr
ip ssh client algorithm encryption aes128-ctr aes192-ctr aes256-ctr
!
!
control-plane
!
!
line con 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line aux 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line vty 0 4
 login
!
!
end
```
```
IOU2#sh run
Building configuration...

Current configuration : 2556 bytes
!
! Last configuration change at 15:19:26 UTC Sun Dec 17 2023
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname IOU2
!
boot-start-marker
boot-end-marker
!
!
logging discriminator EXCESS severity drops 6 msg-body drops EXCESSCOLL
logging buffered 50000
logging console discriminator EXCESS
!
no aaa new-model
!
!
no ip icmp rate-limit unreachable
!
!
no ip domain-lookup
ip cef
no ipv6 cef
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
!
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/1
 switchport access vlan 4
 switchport trunk encapsulation dot1q
 switchport mode access
!
interface Ethernet0/2
!
interface Ethernet0/3
!
interface Ethernet1/0
 switchport access vlan 7
 switchport mode access
!
interface Ethernet1/1
 switchport access vlan 7
 switchport mode access
!
interface Ethernet1/2
 switchport access vlan 7
 switchport mode access
!
interface Ethernet1/3
!
interface Ethernet2/0
 switchport access vlan 8
 switchport mode access
!
interface Ethernet2/1
 switchport access vlan 8
 switchport mode access
!
interface Ethernet2/2
 switchport access vlan 8
 switchport mode access
!
interface Ethernet2/3
 switchport access vlan 2
 switchport mode access
!
interface Ethernet3/0
 switchport access vlan 3
 switchport mode access
!
interface Ethernet3/1
 switchport access vlan 3
 switchport mode access
!
interface Ethernet3/2
!
interface Ethernet3/3
!
interface Ethernet4/0
 switchport access vlan 2
 switchport mode access
!
interface Ethernet4/1
 switchport access vlan 2
 switchport mode access
!
interface Ethernet4/2
!
interface Ethernet4/3
!
interface Ethernet5/0
!
interface Ethernet5/1
!
interface Ethernet5/2
!
interface Ethernet5/3
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan2
 no ip address
!
interface Vlan3
 no ip address
!
interface Vlan4
 no ip address
!
interface Vlan6
 no ip address
!
interface Vlan7
 no ip address
!
interface Vlan8
 no ip address
!
ip forward-protocol nd
!
ip tcp synwait-time 5
ip http server
!
ip ssh server algorithm encryption aes128-ctr aes192-ctr aes256-ctr
ip ssh client algorithm encryption aes128-ctr aes192-ctr aes256-ctr
!
!
control-plane
!
!
line con 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line aux 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line vty 0 4
 login
!
!
end
```
```
IOU7#sh run
Building configuration...

Current configuration : 2267 bytes
!
! Last configuration change at 14:35:47 UTC Sun Dec 17 2023
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname IOU7
!
boot-start-marker
boot-end-marker
!
!
logging discriminator EXCESS severity drops 6 msg-body drops EXCESSCOLL
logging buffered 50000
logging console discriminator EXCESS
!
no aaa new-model
!
!
no ip icmp rate-limit unreachable
!
!
no ip domain-lookup
ip cef
no ipv6 cef
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
!
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/1
 switchport access vlan 5
 switchport mode access
!
interface Ethernet0/2
 switchport access vlan 5
 switchport mode access
!
interface Ethernet0/3
 switchport access vlan 5
 switchport mode access
!
interface Ethernet1/0
 switchport access vlan 5
 switchport mode access
!
interface Ethernet1/1
 switchport access vlan 5
 switchport mode access
!
interface Ethernet1/2
 switchport access vlan 5
 switchport mode access
!
interface Ethernet1/3
 switchport access vlan 5
 switchport mode access
!
interface Ethernet2/0
 switchport access vlan 5
 switchport mode access
!
interface Ethernet2/1
 switchport access vlan 5
 switchport mode access
!
interface Ethernet2/2
 switchport access vlan 5
 switchport mode access
!
interface Ethernet2/3
 switchport access vlan 5
 switchport mode access
!
interface Ethernet3/0
 switchport access vlan 5
 switchport mode access
!
interface Ethernet3/1
 switchport access vlan 5
 switchport mode access
!
interface Ethernet3/2
 switchport access vlan 5
 switchport mode access
!
interface Ethernet3/3
 switchport access vlan 5
 switchport mode access
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan5
 no ip address
!
ip forward-protocol nd
!
ip tcp synwait-time 5
ip http server
!
ip ssh server algorithm encryption aes128-ctr aes192-ctr aes256-ctr
ip ssh client algorithm encryption aes128-ctr aes192-ctr aes256-ctr
!
!
control-plane
!
!
line con 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line aux 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line vty 0 4
 login
!
!
end
```

- Bâtiment 1
```
IOU3#sh run
Building configuration...

Current configuration : 1414 bytes
!
! Last configuration change at 14:59:38 UTC Sun Dec 17 2023
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname IOU3
!
boot-start-marker
boot-end-marker
!
!
logging discriminator EXCESS severity drops 6 msg-body drops EXCESSCOLL
logging buffered 50000
logging console discriminator EXCESS
!
no aaa new-model
!
!
no ip icmp rate-limit unreachable
!
!
no ip domain-lookup
ip cef
no ipv6 cef
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
!
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/1
!
interface Ethernet0/2
!
interface Ethernet0/3
!
interface Ethernet1/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet1/1
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet1/2
!
interface Ethernet1/3
!
interface Vlan1
 no ip address
 shutdown
!
ip forward-protocol nd
!
ip tcp synwait-time 5
ip http server
!
ip ssh server algorithm encryption aes128-ctr aes192-ctr aes256-ctr
ip ssh client algorithm encryption aes128-ctr aes192-ctr aes256-ctr
!
!
control-plane
!
!
line con 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line aux 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line vty 0 4
 login
!
!
end
```
```
IOU4#sh run
Building configuration...

Current configuration : 1933 bytes
!
! Last configuration change at 14:35:40 UTC Sun Dec 17 2023
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname IOU4
!
boot-start-marker
boot-end-marker
!
!
logging discriminator EXCESS severity drops 6 msg-body drops EXCESSCOLL
logging buffered 50000
logging console discriminator EXCESS
!
no aaa new-model
!
!
no ip icmp rate-limit unreachable
!
!
no ip domain-lookup
ip cef
no ipv6 cef
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
!
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/1
!
interface Ethernet0/2
!
interface Ethernet0/3
!
interface Ethernet1/0
 switchport access vlan 8
 switchport mode access
!
interface Ethernet1/1
 switchport access vlan 8
 switchport mode access
!
interface Ethernet1/2
 switchport access vlan 8
 switchport mode access
!
interface Ethernet1/3
 switchport access vlan 6
 switchport mode access
!
interface Ethernet2/0
 switchport access vlan 7
 switchport mode access
!
interface Ethernet2/1
 switchport access vlan 7
 switchport mode access
!
interface Ethernet2/2
 switchport access vlan 7
 switchport mode access
!
interface Ethernet2/3
!
interface Ethernet3/0
!
interface Ethernet3/1
!
interface Ethernet3/2
!
interface Ethernet3/3
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan6
 no ip address
!
interface Vlan7
 no ip address
!
interface Vlan8
 no ip address
!
ip forward-protocol nd
!
ip tcp synwait-time 5
ip http server
!
ip ssh server algorithm encryption aes128-ctr aes192-ctr aes256-ctr
ip ssh client algorithm encryption aes128-ctr aes192-ctr aes256-ctr
!
!
control-plane
!
!
line con 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line aux 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line vty 0 4
 login
!
!
end
```
```
IOU5#sh run
Building configuration...

Current configuration : 2267 bytes
!
! Last configuration change at 14:35:39 UTC Sun Dec 17 2023
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname IOU5
!
boot-start-marker
boot-end-marker
!
!
logging discriminator EXCESS severity drops 6 msg-body drops EXCESSCOLL
logging buffered 50000
logging console discriminator EXCESS
!
no aaa new-model
!
!
no ip icmp rate-limit unreachable
!
!
no ip domain-lookup
ip cef
no ipv6 cef
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
!
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/1
 switchport access vlan 4
 switchport mode access
!
interface Ethernet0/2
 switchport access vlan 4
 switchport mode access
!
interface Ethernet0/3
 switchport access vlan 4
 switchport mode access
!
interface Ethernet1/0
 switchport access vlan 4
 switchport mode access
!
interface Ethernet1/1
 switchport access vlan 4
 switchport mode access
!
interface Ethernet1/2
 switchport access vlan 4
 switchport mode access
!
interface Ethernet1/3
 switchport access vlan 4
 switchport mode access
!
interface Ethernet2/0
 switchport access vlan 4
 switchport mode access
!
interface Ethernet2/1
 switchport access vlan 4
 switchport mode access
!
interface Ethernet2/2
 switchport access vlan 4
 switchport mode access
!
interface Ethernet2/3
 switchport access vlan 4
 switchport mode access
!
interface Ethernet3/0
 switchport access vlan 4
 switchport mode access
!
interface Ethernet3/1
 switchport access vlan 4
 switchport mode access
!
interface Ethernet3/2
 switchport access vlan 4
 switchport mode access
!
interface Ethernet3/3
 switchport access vlan 4
 switchport mode access
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan4
 no ip address
!
ip forward-protocol nd
!
ip tcp synwait-time 5
ip http server
!
ip ssh server algorithm encryption aes128-ctr aes192-ctr aes256-ctr
ip ssh client algorithm encryption aes128-ctr aes192-ctr aes256-ctr
!
!
control-plane
!
!
line con 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line aux 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line vty 0 4
 login
!
!
end
```
```
IOU6#sh run
Building configuration...

Current configuration : 2267 bytes
!
! Last configuration change at 14:35:44 UTC Sun Dec 17 2023
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname IOU6
!
boot-start-marker
boot-end-marker
!
!
logging discriminator EXCESS severity drops 6 msg-body drops EXCESSCOLL
logging buffered 50000
logging console discriminator EXCESS
!
no aaa new-model
!
!
no ip icmp rate-limit unreachable
!
!
no ip domain-lookup
ip cef
no ipv6 cef
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
!
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/1
 switchport access vlan 5
 switchport mode access
!
interface Ethernet0/2
 switchport access vlan 5
 switchport mode access
!
interface Ethernet0/3
 switchport access vlan 5
 switchport mode access
!
interface Ethernet1/0
 switchport access vlan 5
 switchport mode access
!
interface Ethernet1/1
 switchport access vlan 5
 switchport mode access
!
interface Ethernet1/2
 switchport access vlan 5
 switchport mode access
!
interface Ethernet1/3
 switchport access vlan 5
 switchport mode access
!
interface Ethernet2/0
 switchport access vlan 5
 switchport mode access
!
interface Ethernet2/1
 switchport access vlan 5
 switchport mode access
!
interface Ethernet2/2
 switchport access vlan 5
 switchport mode access
!
interface Ethernet2/3
 switchport access vlan 5
 switchport mode access
!
interface Ethernet3/0
 switchport access vlan 5
 switchport mode access
!
interface Ethernet3/1
 switchport access vlan 5
 switchport mode access
!
interface Ethernet3/2
 switchport access vlan 5
 switchport mode access
!
interface Ethernet3/3
 switchport access vlan 5
 switchport mode access
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan5
 no ip address
!
ip forward-protocol nd
!
ip tcp synwait-time 5
ip http server
!
ip ssh server algorithm encryption aes128-ctr aes192-ctr aes256-ctr
ip ssh client algorithm encryption aes128-ctr aes192-ctr aes256-ctr
!
!
control-plane
!
!
line con 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line aux 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line vty 0 4
 login
!
!
end
```

- **DHCP**
```
[robot-045@dhcp ~]$sudo dnf install -y dhcp
```
```
[robot-045@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
# create new

default-lease-time 600;
max-lease-time 7200;
authoritative;

subnet 10.2.40.0 netmask 255.255.255.0 {
  range 10.2.40.10 10.2.40.200;
  option routers 10.2.40.1;
  option domain-name-servers 1.1.1.1;
}

subnet 10.2.50.0 netmask 255.255.254.0 {
  range 10.2.50.10 10.2.50.250;
  option routers 10.2.50.1;
  option domain-name-servers 1.1.1.1;
}
EOF;
```
