# III. Héberger

---
## Sommaire

- [III. Héberger](#iii-héberger)
  - [Sommaire](#sommaire)
  - [1. Interface bridge](#1-interface-bridge)
  - [2. Firewall](#2-firewall)
  - [3. Serveur SSH](#3-serveur-ssh)
  - [4. Serveur Calculatrice](#4-serveur-calculatrice)

---
## 1. Interface bridge

---
## 2. Firewall

🤖 **Assurez-vous qu'aucun port est inutilement ouvert**

```
[robot-045@hosting ~]$ sudo firewall-cmd --list-port
13337/tcp 19999/tcp
```

---
## 3. Serveur SSH

🤖 **Conf serveur SSH**

- éditer la configuration du serveur SSH `/etc/ssh/sshd_config`
```
[robot-045@hosting ~]$ sudo nano /etc/ssh/sshd_config
[robot-045@hosting calculatrice]$ sudo cat /etc/ssh/sshd_config | grep ListenAddress
ListenAddress 10.0.2.15
```
```
[robot-045@hosting ~]$ sudo systemctl restart sshd
```
- prouvez avec un `ss` que ça a pris effet
```
[robot-045@hosting calculatrice]$ ss -tnl | grep 10.0.2.15
LISTEN 0      128        10.0.2.15:22         0.0.0.0:*
```

---
## 4. Serveur Calculatrice

🤖 **Conf serveur Calculatrice**

```
[robot-045@hosting calculatrice]$ sudo nano server.py

[robot-045@hosting calculatrice]$ sudo systemctl restart calculatrice
```

- prouvez avec un `ss` que le changement a bien pris effet
```
[robot-045@hosting calculatrice]$ ss -tnl | grep 10.0.2.15
LISTEN 0      128        10.0.2.15:22         0.0.0.0:*
LISTEN 0      1          10.0.2.15:13337      0.0.0.0:*
```