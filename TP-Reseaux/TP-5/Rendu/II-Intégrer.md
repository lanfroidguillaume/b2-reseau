# II. Intégrer

---
## Sommaire

- [II. Intégrer](#ii-intégrer)
  - [Sommaire](#sommaire)
  - [1. Environnement](#1-environnement)
  - [2. systemd service](#2-systemd-service)
    - [B. Service basique](#b-service-basique)
    - [C. Amélioration du service](#c-amélioration-du-service)
  - [3. Monitoring](#3-monitoring)

---
## 1. Environnement

🤖 **Créer un dossier /opt/calculatrice**

```
[robot-045@hosting ~]$ sudo mkdir /opt/calculatrice
```
- il doit contenir le code de l'application
```
[robot-045@hosting ~]$ sudo mv python_app/server.py /opt/calculatrice/
```

---
## 2. systemd service

### B. Service basique

🤖 **Créer le fichier `/etc/systemd/system/calculatrice.service`**

- vous devrez apposer des permissions correctes sur ce fichier
```
[robot-045@hosting system]$ sudo chmod --reference=dbus-org.fedoraproject.FirewallD1.service calculatrice.service
```
- un contenu minimal serait :
```
[robot-045@hosting ~]$ sudo cat /etc/systemd/system/calculatrice.service
[Unit]
Description=Super calculatrice réseau

[Service]
ExecStart=/usr/bin/python3 /opt/calculatrice/server.py

[Install]
WantedBy=multi-user.target

```

🤖 **Démarrer le service**

- avec une commande `sudo systemctl start calculatrice`
- prouvez que...
  - le service est actif avec un `systemctl status`
```
[robot-045@hosting system]$ sudo systemctl status calculatrice
● calculatrice.service - Super calculatrice réseau
   Loaded: loaded (/etc/systemd/system/calculatrice.service; indirect; vendor preset: disabled)
   Active: active (running) since Tue 2023-11-28 11:45:36 CET; 3min 5s ago
 Main PID: 2664 (python3)
    Tasks: 1 (limit: 4621)
   Memory: 7.5M
   CGroup: /system.slice/calculatrice.service
           └─2664 /usr/bin/python3 /opt/calculatrice/server.py

Nov 28 11:45:36 hosting.tp5.b2 systemd[1]: Started Super calculatrice réseau.
Nov 28 11:45:36 hosting.tp5.b2 python3[2664]: Le serveur tourne sur 10.1.10.4:13337
```
  - le service tourne derrière un port donné avec un `ss`
```
[robot-045@hosting python_app]$ ss -ltn
State             Recv-Q            Send-Q                       Local Address:Port                         Peer Address:Port            Process
LISTEN            0                 128                                0.0.0.0:22                                0.0.0.0:*
LISTEN            0                 1                                10.1.10.4:13337                             0.0.0.0:*
LISTEN            0                 128                                   [::]:22                                   [::]:*
```
  - c'est fonctionnel : vous pouvez utiliser l'app en lançant le client
```
[robot-045@hosting python_app]$ sudo python3 client.py
Veuillez saisir une opération arithmétique : 10 + 10
```


### C. Amélioration du service

🤖 **Configurer une politique de redémarrage** dans le fichier `calculatrice.service`

- un `cat` du fichier pour le compte-rendu
```
[robot-045@hosting system]$ sudo cat calculatrice.service
[Unit]
Description=Super calculatrice réseau

[Service]
ExecStart=/usr/bin/python3 /opt/calculatrice/server.py
Restart=always
RestartSec=30

[Install]
WantedBy=multi-user.target

```

🤖 **Tester que la politique de redémarrage fonctionne**

- lancez le service, repérer son PID, et tuer le process avec un `kill -9 <PID>`
- attendre et vérifier que le service redémarre
```
[robot-045@hosting system]$ sudo systemctl status calculatrice
● calculatrice.service - Super calculatrice réseau
   Loaded: loaded (/etc/systemd/system/calculatrice.service; indirect; vendor preset: disabled)
   Active: active (running) since Tue 2023-11-28 12:20:48 CET; 1s ago
 Main PID: 2939 (python3)
    Tasks: 2 (limit: 4621)
   Memory: 5.7M
   CGroup: /system.slice/calculatrice.service
           └─2939 /usr/bin/python3 /opt/calculatrice/server.py

Nov 28 12:20:48 hosting.tp5.b2 systemd[1]: Started Super calculatrice réseau.
Nov 28 12:20:48 hosting.tp5.b2 python3[2939]: Le serveur tourne sur 10.1.10.4:13337
[robot-045@hosting system]$ ps -ef | grep calculatrice
root        2939       1  0 12:20 ?        00:00:00 /usr/bin/python3 /opt/calculatrice/server.py
robot-0+    2964    1951  0 12:21 pts/2    00:00:00 grep --color=auto calculatrice
[robot-045@hosting system]$ sudo kill 2939
[robot-045@hosting system]$ sudo systemctl status calculatrice
● calculatrice.service - Super calculatrice réseau
   Loaded: loaded (/etc/systemd/system/calculatrice.service; indirect; vendor preset: disabled)
   Active: activating (auto-restart) since Tue 2023-11-28 12:21:08 CET; 7s ago
 Main PID: 2939 (code=killed, signal=TERM)
    Tasks: 0 (limit: 4621)
   Memory: 0B
   CGroup: /system.slice/calculatrice.service

Nov 28 12:21:08 hosting.tp5.b2 systemd[1]: calculatrice.service: Succeeded.
[robot-045@hosting system]$ sudo systemctl status calculatrice
● calculatrice.service - Super calculatrice réseau
   Loaded: loaded (/etc/systemd/system/calculatrice.service; indirect; vendor preset: disabled)
   Active: active (running) since Tue 2023-11-28 12:21:38 CET; 1min 42s ago
 Main PID: 2990 (python3)
    Tasks: 1 (limit: 4621)
   Memory: 7.4M
   CGroup: /system.slice/calculatrice.service
           └─2990 /usr/bin/python3 /opt/calculatrice/server.py

Nov 28 12:21:38 hosting.tp5.b2 systemd[1]: calculatrice.service: Scheduled restart job, restart counter is at 3.
Nov 28 12:21:38 hosting.tp5.b2 systemd[1]: Stopped Super calculatrice réseau.
Nov 28 12:21:38 hosting.tp5.b2 systemd[1]: Started Super calculatrice réseau.
Nov 28 12:21:38 hosting.tp5.b2 python3[2990]: Le serveur tourne sur 10.1.10.4:13337
```

➜ **Firewall !**

🤖 **Ouverture automatique du firewall** dans le fichier `calculatrice.service`

- un `cat` du fichier pour le compte-rendu
```
[robot-045@hosting ~]$ cat /etc/systemd/system/calculatrice.service
[Unit]
Description=Super calculatrice réseau

[Service]
ExecStartPre=/usr/bin/firewall-cmd --add-port=13337/tcp
ExecStart=/usr/bin/python3 /opt/calculatrice/server.py
ExecStopPost=/usr/bin/sudo /usr/bin/firewall-cmd --remove-port=13337/tcp
Restart=always
RestartSec=30

[Install]
WantedBy=multi-user.target
```

🤖 **Vérifier l'ouverture automatique du firewall**

- lancer/stopper le service et constater avec un `firewall-cmd --list-all` que le port s'ouvre/se ferme bien automatiquement
```
[robot-045@hosting system]$ sudo systemctl stop calculatrice
[robot-045@hosting system]$ sudo firewall-cmd --list-port

[robot-045@hosting system]$ sudo systemctl start calculatrice
[robot-045@hosting system]$ sudo firewall-cmd --list-port
13337/tcp
```
- tester une connexion depuis **votre PC**
```
PS C:\Users\lanfr> & C:/Users/lanfr/AppData/Local/Microsoft/WindowsApps/python3.10.exe c:/Users/lanfr/OneDrive/Bureau/python_app/client.py
Veuillez saisir une opération arithmétique : 10+10
```
```
2023-11-30 09:25:22 INFO Connexion réussie à 10.1.10.4:13337
2023-11-30 09:25:24 INFO Message envoyé au serveur 10.1.10.4 : 10+10
2023-11-30 09:25:24 INFO Réponse reçue du serveur 10.1.10.4 : b'20'
```

---
## 3. Monitoring

🤖 **Installer Netdata** sur `hosting.tp5.b1`

```
curl https://my-netdata.io/kickstart.sh > /tmp/netdata-kickstart.sh && sh /tmp/netdata-kickstart.sh --stable-channel --claim-token 1uAFGA0mKPMCsztQqtDP1Gz87XW576eyDaEdhCJnhaWeyVGl8B5RSEK64Mcg8FyW5QP4zQ7l0bvC4UDBbG93Xn7hmT7BF3BlKj2Z12FwCRm4tAvV1CZU0vMzDeFrN6B-EmTLLHg --claim-rooms b38c55b6-a4e2-4b8f-94a5-6f2ede75ccba
```
```
[robot-045@hosting ~]$ systemctl status netdata.service
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: enabled)
   Active: active (running) since Thu 2023-11-30 11:25:01 CET; 6min ago
  Process: 3617 ExecStartPre=/bin/chown -R netdata /run/netdata (code=exited, status=0/SUCCESS)
  Process: 3615 ExecStartPre=/bin/mkdir -p /run/netdata (code=exited, status=0/SUCCESS)
  Process: 3614 ExecStartPre=/bin/chown -R netdata /var/cache/netdata (code=exited, status=0/SUCCESS)
  Process: 3612 ExecStartPre=/bin/mkdir -p /var/cache/netdata (code=exited, status=0/SUCCESS)
```
```
[robot-045@hosting netdata]$ sudo firewall-cmd --add-port=19999/tcp --permanent
[robot-045@hosting ~]$ sudo firewall-cmd --list-port
13337/tcp 19999/tcp
```

🤖 **Configurer une sonde TCP**

```
[robot-045@hosting ~]$ cd /etc/netdata 2>/dev/null || cd /opt/netdata/etc/netdata
[robot-045@hosting netdata]$ sudo nano go.d/portcheck.conf
[robot-045@hosting netdata]$ cat go.d/portcheck.conf
## All available configuration options, their descriptions and default values:
## https://github.com/netdata/go.d.plugin/tree/master/modules/portcheck

#update_every: 1
#autodetection_retry: 0
#priority: 70000

```
```
[robot-045@hosting netdata]$ cd /etc/netdata 2>/dev/null || cd /opt/netdata/etc/netdata
[robot-045@hosting netdata]$ sudo ./edit-config go.d/portcheck.conf
[robot-045@hosting netdata]$ cat go.d/portcheck.conf
## All available configuration options, their descriptions and default values:
## https://github.com/netdata/go.d.plugin/tree/master/modules/portcheck

#update_every: 1
#autodetection_retry: 0
#priority: 70000

jobs:
 - name: Serveur
   host: 10.1.10.4
   ports: 13337

# - name: job2
#   host: 10.0.0.2
#   ports: [22, 19999]
```
```
[robot-045@hosting netdata]$ sudo systemctl restart netdata.service
```

🤖 **Alerting Discord**

```
[robot-045@hosting netdata]$cd /etc/netdata 2>/dev/null || cd /opt/netdata/etc/netdata
[robot-045@hosting netdata]$sudo ./edit-config health_alarm_notify.conf
[robot-045@hosting netdata]$ cd /etc/netdata 2>/dev/null || cd /opt/netdata/etc/netdata
[robot-045@hosting netdata]$ cat health_alarm_notify.conf
#------------------------------------------------------------------------------
# discord (discordapp.com) global notification options

SEND_DISCORD="YES"
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/1179733234834219008/cOSFFdaQGZOQQlzcNGvNUDEpPycEpTnkMTKvx_6MeWjq7uc24yton8FztXbjoL3RUr2K"
DEFAULT_RECIPIENT_DISCORD="alerts"
```
