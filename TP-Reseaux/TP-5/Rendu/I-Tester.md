## I. Tester 

🤖 **Récupérer l'application dans la VM `hosting.tp5.b1`**

- Recupération du dossier

🤖 **Essayer de lancer l'app**

```
[robot-045@hosting python_app]$ sudo python3 server.py
13337
Le serveur tourne sur 10.1.10.4:13337

[robot-045@hosting python_app]$ ss -ltn
State           Recv-Q          Send-Q                   Local Address:Port                     Peer Address:Port          Process
LISTEN          0               128                            0.0.0.0:22                            0.0.0.0:*
LISTEN          0               1                            10.1.10.4:13337                         0.0.0.0:*
LISTEN          0               128                               [::]:22                               [::]:*
```

🤖 **Tester l'app depuis `hosting.tp5.b1`**

```
[robot-045@hosting python_app]$ sudo python3 client.py
Veuillez saisir une opération arithmétique : 10-2
```
```
Un client ('10.1.10.4', 34356) s'est connecté.
Le client ('10.1.10.4', 34356) a envoyé 10-2
Réponse envoyée au client ('10.1.10.4', 34356) : 8.
```