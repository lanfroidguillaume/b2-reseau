# TP5 : Intégration

---
## I. Tester

[I. Tester.](./Rendu/I-Tester.md)

## II. Intégrer

[II. Intégrer.](./Rendu/II-Intégrer.md)

## III. Héberger

[III. Héberger.](./Rendu/III-Héberger.md)
