# TP3 INFRA : Premiers pas GNS, Cisco et VLAN 

# Sommaire

- [TP3 INFRA : Premiers pas GNS, Cisco et VLAN](#tp3-infra--premiers-pas-gns-cisco-et-vlan)
- [Sommaire](#sommaire)
- [I. Dumb switch](#i-dumb-switch)
- [II. VLAN](#ii-vlan)
- [III. Ptite VM DHCP](#iii-ptite-vm-dhcp)

# I. Dumb switch
---
🤖 **Commençons simple**

- définissez les IPs statiques sur les deux VPCS
```
PC1> ip 10.3.1.1 255.255.255.0
Checking for duplicate address...
PC1 : 10.3.1.1 255.255.255.0

PC1> sh

NAME   IP/MASK              GATEWAY           MAC                LPORT  RHOST:PORT
PC1    10.3.1.1/24          255.255.255.0     00:50:79:66:68:01  20006  127.0.0.1:20007
       fe80::250:79ff:fe66:6801/64
```
```
PC2> ip 10.3.1.2 255.255.255.0
Checking for duplicate address...
PC2 : 10.3.1.2 255.255.255.0

PC2> sh

NAME   IP/MASK              GATEWAY           MAC                LPORT  RHOST:PORT
PC2    10.3.1.2/24          255.255.255.0     00:50:79:66:68:00  20004  127.0.0.1:20005
       fe80::250:79ff:fe66:6800/64
```

- `ping` un VPCS depuis l'autre
```
PC1> ping 10.3.1.2

84 bytes from 10.3.1.2 icmp_seq=1 ttl=64 time=0.586 ms
84 bytes from 10.3.1.2 icmp_seq=2 ttl=64 time=0.889 ms
84 bytes from 10.3.1.2 icmp_seq=3 ttl=64 time=0.830 ms
84 bytes from 10.3.1.2 icmp_seq=4 ttl=64 time=0.662 ms
84 bytes from 10.3.1.2 icmp_seq=5 ttl=64 time=0.360 ms
```
```
PC2> ping 10.3.1.1

84 bytes from 10.3.1.1 icmp_seq=1 ttl=64 time=2.212 ms
84 bytes from 10.3.1.1 icmp_seq=2 ttl=64 time=2.501 ms
84 bytes from 10.3.1.1 icmp_seq=3 ttl=64 time=2.571 ms
84 bytes from 10.3.1.1 icmp_seq=4 ttl=64 time=1.580 ms
84 bytes from 10.3.1.1 icmp_seq=5 ttl=64 time=2.329 ms
```
- le passage par le switch
```
IOU3#show mac address-table
          Mac Address Table
-------------------------------------------

Vlan    Mac Address       Type        Ports
----    -----------       --------    -----
   1    0050.7966.6800    DYNAMIC     Et0/2
   1    0050.7966.6801    DYNAMIC     Et0/1
Total Mac Addresses for this criterion: 2
```


# II. VLAN
---
🤖 **Adressage**

- définissez les IPs statiques sur tous les VPCS
```
PC1> ip 10.3.1.1 255.255.255.0
Checking for duplicate address...
PC1 : 10.3.1.1 255.255.255.0

PC1> sh

NAME   IP/MASK              GATEWAY           MAC                LPORT  RHOST:PORT
PC1    10.3.1.1/24          255.255.255.0     00:50:79:66:68:02  20014  127.0.0.1:20015
       fe80::250:79ff:fe66:6802/64
```
```
PC2> ip 10.3.1.2 255.255.255.0
Checking for duplicate address...
PC2 : 10.3.1.2 255.255.255.0

PC2> sh

NAME   IP/MASK              GATEWAY           MAC                LPORT  RHOST:PORT
PC2    10.3.1.2/24          255.255.255.0     00:50:79:66:68:03  20016  127.0.0.1:20017
       fe80::250:79ff:fe66:6803/64
```
```
PC3> ip 10.3.1.3 255.255.255.0
Checking for duplicate address...
PC3 : 10.3.1.3 255.255.255.0

PC3> sh

NAME   IP/MASK              GATEWAY           MAC                LPORT  RHOST:PORT
PC3    10.3.1.3/24          255.255.255.0     00:50:79:66:68:04  20018  127.0.0.1:20019
       fe80::250:79ff:fe66:6804/64
```
- vérifiez avec des `ping` que tout le monde se ping
```
PC1> ping 10.3.1.2

84 bytes from 10.3.1.2 icmp_seq=1 ttl=64 time=1.266 ms
84 bytes from 10.3.1.2 icmp_seq=2 ttl=64 time=1.732 ms
84 bytes from 10.3.1.2 icmp_seq=3 ttl=64 time=1.285 ms
84 bytes from 10.3.1.2 icmp_seq=4 ttl=64 time=1.897 ms
84 bytes from 10.3.1.2 icmp_seq=5 ttl=64 time=1.777 ms

PC1> ping 10.3.1.3

84 bytes from 10.3.1.3 icmp_seq=1 ttl=64 time=1.124 ms
84 bytes from 10.3.1.3 icmp_seq=2 ttl=64 time=0.908 ms
84 bytes from 10.3.1.3 icmp_seq=3 ttl=64 time=1.785 ms
84 bytes from 10.3.1.3 icmp_seq=4 ttl=64 time=1.628 ms
84 bytes from 10.3.1.3 icmp_seq=5 ttl=64 time=2.140 ms
```
```
PC2> ping 10.3.1.1

84 bytes from 10.3.1.1 icmp_seq=1 ttl=64 time=2.748 ms
84 bytes from 10.3.1.1 icmp_seq=2 ttl=64 time=1.678 ms
84 bytes from 10.3.1.1 icmp_seq=3 ttl=64 time=0.700 ms
84 bytes from 10.3.1.1 icmp_seq=4 ttl=64 time=2.769 ms
84 bytes from 10.3.1.1 icmp_seq=5 ttl=64 time=1.122 ms

PC2> ping 10.3.1.3

84 bytes from 10.3.1.3 icmp_seq=1 ttl=64 time=2.497 ms
84 bytes from 10.3.1.3 icmp_seq=2 ttl=64 time=2.102 ms
84 bytes from 10.3.1.3 icmp_seq=3 ttl=64 time=2.036 ms
84 bytes from 10.3.1.3 icmp_seq=4 ttl=64 time=2.305 ms
84 bytes from 10.3.1.3 icmp_seq=5 ttl=64 time=1.454 ms
```
```
PC3> ping 10.3.1.1

84 bytes from 10.3.1.1 icmp_seq=1 ttl=64 time=2.391 ms
84 bytes from 10.3.1.1 icmp_seq=2 ttl=64 time=1.837 ms
84 bytes from 10.3.1.1 icmp_seq=3 ttl=64 time=2.224 ms
84 bytes from 10.3.1.1 icmp_seq=4 ttl=64 time=1.190 ms
84 bytes from 10.3.1.1 icmp_seq=5 ttl=64 time=0.622 ms

PC3> ping 10.3.1.2

84 bytes from 10.3.1.2 icmp_seq=1 ttl=64 time=1.233 ms
84 bytes from 10.3.1.2 icmp_seq=2 ttl=64 time=2.119 ms
84 bytes from 10.3.1.2 icmp_seq=3 ttl=64 time=1.905 ms
84 bytes from 10.3.1.2 icmp_seq=4 ttl=64 time=3.222 ms
84 bytes from 10.3.1.2 icmp_seq=5 ttl=64 time=2.078 ms
```

---
🤖 **Configuration des VLANs**

- déclaration des VLANs sur le switch `sw1`
```
IOU1#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
IOU1(config)#vlan 10
IOU1(config-vlan)#name C10
IOU1(config-vlan)#exit
IOU1(config)#vlan 20
IOU1(config-vlan)#name C20
IOU1(config-vlan)#exit
IOU1(config)#exit
IOU1#
```
- ajout des ports du switches dans le bon VLAN (voir le tableau d'adressage de la topo 2 juste au dessus
```
IOU1#conf t
IOU1(config)#interface Vlan10
IOU1(config-if)#no shutdown
IOU1(config-if)#interface Ethernet0/1
IOU1(config-if)#switchport mode access
IOU1(config-if)#switchport access vlan 10
IOU1(config-if)#exit
IOU1(config)#interface Ethernet0/2
IOU1(config-if)#switchport mode access
IOU1(config-if)#switchport access vlan 10
IOU1(config-if)#exit
IOU1(config)#exit
IOU1#conf t
IOU1(config)#interface Vlan20
IOU1(config-if)#no shutdown
IOU1(config-if)#interface Ethernet0/3
IOU1(config-if)#switchport mode access
IOU1(config-if)#switchport access vlan 20
IOU1(config-if)#exit
IOU1(config)#exit
```
```
IOU1#show mac address-table
          Mac Address Table
-------------------------------------------

Vlan    Mac Address       Type        Ports
----    -----------       --------    -----
  10    0050.7966.6802    DYNAMIC     Et0/1
  10    0050.7966.6803    DYNAMIC     Et0/2
  20    0050.7966.6804    DYNAMIC     Et0/3
Total Mac Addresses for this criterion: 3

```

---
🤖 **Vérif**

- `pc1` et `pc2` doivent toujours pouvoir se ping
```
PC1> ping 10.3.1.2

84 bytes from 10.3.1.2 icmp_seq=1 ttl=64 time=1.529 ms
84 bytes from 10.3.1.2 icmp_seq=2 ttl=64 time=2.296 ms
84 bytes from 10.3.1.2 icmp_seq=3 ttl=64 time=1.772 ms
84 bytes from 10.3.1.2 icmp_seq=4 ttl=64 time=1.669 ms
84 bytes from 10.3.1.2 icmp_seq=5 ttl=64 time=2.128 ms

PC2> ping 10.3.1.1

84 bytes from 10.3.1.1 icmp_seq=1 ttl=64 time=1.245 ms
84 bytes from 10.3.1.1 icmp_seq=2 ttl=64 time=1.736 ms
84 bytes from 10.3.1.1 icmp_seq=3 ttl=64 time=0.491 ms
84 bytes from 10.3.1.1 icmp_seq=4 ttl=64 time=0.578 ms
84 bytes from 10.3.1.1 icmp_seq=5 ttl=64 time=2.616 ms
```
- `pc3` ne ping plus personne
```
PC1> ping 10.3.1.3

host (10.3.1.3) not reachable
```
```
PC2> ping 10.3.1.3

host (10.3.1.3) not reachable
```

# III. Ptite VM DHCP
---
🤖 **VM `dhcp.tp3.b2`**

- installez un serveur DHCP
```
[robot-045@dhcp ~]$ sudo dnf -y install dhcp-server
[...]
Complete!

```
- conf
```
[robot-045@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
# create new
# specify domain name
# specify DNS server's hostname or IP address
option domain-name-servers 1.1.1.1, 8.8.8.8;
# default lease time
default-lease-time 600;
# max lease time
max-lease-time 7200;
# this DHCP server to be declared valid
authoritative;
# specify network address and subnetmask
subnet 10.3.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range 10.3.1.100 10.3.1.200;
    # specify broadcast address
    option broadcast-address 10.3.1.255;
    # specify gateway
    option routers 10.3.1.1;
}
```
- status
```
[robot-045@dhcp ~]$ sudo systemctl status dhcpd
● dhcpd.service - DHCPv4 Server Daemon
   Loaded: loaded (/usr/lib/systemd/system/dhcpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2023-10-26 17:55:56 CEST; 2min 25s ago
[...]
```
- vérifier avec le `pc4` que vous pouvez récupérer une IP en DHCP
```
PC4> show

NAME   IP/MASK              GATEWAY           MAC                LPORT  RHOST:PORT
PC4    10.3.1.100/24        10.3.1.1          00:50:79:66:68:08  20036  127.0.0.1:20037
       fe80::250:79ff:fe66:6808/64
```
- vérifier avec le `pc5` que vous ne pouvez PAS récupérer une IP en DHCP
```
PC5> sh

NAME   IP/MASK              GATEWAY           MAC                LPORT  RHOST:PORT
PC5    0.0.0.0/0            0.0.0.0           00:50:79:66:68:09  20032  127.0.0.1:20033
       fe80::250:79ff:fe66:6809/64
```
