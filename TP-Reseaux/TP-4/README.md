# TP4 : Router-on-a-stick

# Sommaire

- [TP4 : Router-on-a-stick](#tp4--router-on-a-stick)
- [Sommaire](#sommaire)
- [I. VLAN et Routing](#i-vlan-et-routing)
- [II. NAT](#ii-nat)
- [III. Add a building](#iii-add-a-building)

---
# I. VLAN et Routing

🤖 **Adressage**

- définissez les IPs statiques sur toutes les machines **sauf le *routeur***
```
PC1> sh ip
NAME        : PC1[1]
IP/MASK     : 10.1.10.1/24
GATEWAY     : 255.255.255.0
DNS         :
MAC         : 00:50:79:66:68:00
LPORT       : 20011
RHOST:PORT  : 127.0.0.1:20012
MTU         : 
```
```
PC2> sh ip
NAME        : PC2[1]
IP/MASK     : 10.1.10.2/24
GATEWAY     : 255.255.255.0
DNS         :
MAC         : 00:50:79:66:68:01
LPORT       : 20013
RHOST:PORT  : 127.0.0.1:20014
MTU         : 1500
```
```
adm1> sh ip

NAME        : adm1[1]
IP/MASK     : 10.1.20.1/24
GATEWAY     : 255.255.255.0
DNS         :
MAC         : 00:50:79:66:68:02
LPORT       : 20015
RHOST:PORT  : 127.0.0.1:20016
MTU         : 1500

```

🤖 **Configuration des VLANs**

- déclaration des VLANs sur le switch `
```
IOU1#conf t
IOU1(config)#vlan 10
IOU1(config-vlan)#name clients
IOU1(config-vlan)#exit
IOU1(config)#vlan 20
IOU1(config-vlan)#name admins
IOU1(config-vlan)#exit
IOU1(config)#vlan 30
IOU1(config-vlan)#name servers
IOU1(config-vlan)#end
IOU1#
```
```
IOU1#show vlan
VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Et0/0, Et0/1, Et0/2, Et0/3
                                                Et1/0, Et1/1, Et1/2, Et1/3
                                                Et2/0, Et2/1, Et2/2, Et2/3
                                                Et3/0, Et3/1, Et3/2, Et3/3
10   clients                          active
20   admins                           active
30   servers                          active
```
- ajout des ports du switches dans le bon VLAN 
```
sw1#conf t
sw1(config)#interface Vlan10
sw1(config-if)#no shutdown
sw1(config-if)#interface Ethernet0/1
sw1(config-if)#switchport mode access
sw1(config-if)#switchport access vlan 10
sw1(config-if)#exit
sw1(config)#interface Vlan10
sw1(config-if)#interface Ethernet0/2
sw1(config-if)#switchport mode access
sw1(config-if)#switchport access vlan 10
sw1(config-if)#exit
sw1(config)#interface Vlan20
sw1(config-if)#interface Ethernet0/3
sw1(config-if)#switchport mode access
sw1(config-if)#switchport access vlan 20
sw1(config-if)#exit
sw1(config)#interface Vlan30
sw1(config-if)#interface Ethernet1/0
sw1(config-if)#switchport mode access
sw1(config-if)#switchport access vlan 30
sw1(config-if)#exit
sw1(config)#exit
sw1#
```
- il faudra ajouter le port qui pointe vers le *routeur* comme un *trunk* : c'est un port entre deux équipements réseau (un *switch* et un *routeur*)
```
sw1#conf t
sw1(config)#interface Ethernet0/0
sw1(config-if)#switchport trunk encapsulation dot1q
sw1(config-if)#switchport mode trunk
sw1(config-if)#switchport trunk allowed vlan add 10,20,30
sw1(config-if)#exit
sw1(config)#exit
sw1#
```
```
IOU1#conf t
*Nov  7 09:28:57.351: %SYS-5-CONFIG_I: Configured from console by console
IOU1#show vlan
VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Et1/1, Et1/2, Et1/3, Et2/0
                                                Et2/1, Et2/2, Et2/3, Et3/0
                                                Et3/1, Et3/2, Et3/3
10   clients                          active    Et0/1, Et0/2
20   admins                           active    Et0/3
30   servers                          active    Et1/0
```

🤖 **Config du *routeur***

- attribuez ses IPs au *routeur*
```
R2#conf t
R2(config)#interface fastEthernet 0/0.10
R2(config-subif)#encapsulation dot1Q 10
R2(config-subif)#ip addr 10.1.10.254 255.255.255.0
R2(config-subif)#exit
R2(config)#interface fastEthernet 0/0.20
R2(config-subif)#encapsulation dot1Q 20
R2(config-subif)#ip addr 10.1.20.254 255.255.255.0
R2(config-subif)#exit
R2(config)#interface fastEthernet 0/0.30
R2(config-subif)#encapsulation dot1Q 30
R2(config-subif)#ip addr 10.1.30.254 255.255.255.0
R2(config-subif)#exit
R2(config)#exit
R2#
```
```
R1#conf t
R1(config)#interface fastEthernet 0/0
R1(config-if)#no shutdown
R1(config-if)#end
R1#
```

🌞 **Vérif**

- tout le monde doit pouvoir ping le routeur sur l'IP qui est dans son réseau
```
PC1> ping 10.1.10.254
84 bytes from 10.1.10.254 icmp_seq=1 ttl=255 time=10.616 ms
84 bytes from 10.1.10.254 icmp_seq=2 ttl=255 time=11.345 ms
84 bytes from 10.1.10.254 icmp_seq=3 ttl=255 time=7.700 ms
84 bytes from 10.1.10.254 icmp_seq=4 ttl=255 time=2.009 ms
84 bytes from 10.1.10.254 icmp_seq=5 ttl=255 time=3.395 ms
```
```
PC2> ping 10.1.10.254
84 bytes from 10.1.10.254 icmp_seq=1 ttl=255 time=9.681 ms
84 bytes from 10.1.10.254 icmp_seq=2 ttl=255 time=2.144 ms
84 bytes from 10.1.10.254 icmp_seq=3 ttl=255 time=4.912 ms
84 bytes from 10.1.10.254 icmp_seq=4 ttl=255 time=1.485 ms
84 bytes from 10.1.10.254 icmp_seq=5 ttl=255 time=5.316 ms
```
```
adm1> ping 10.1.20.254
84 bytes from 10.1.20.254 icmp_seq=1 ttl=255 time=19.748 ms
84 bytes from 10.1.20.254 icmp_seq=2 ttl=255 time=9.435 ms
84 bytes from 10.1.20.254 icmp_seq=3 ttl=255 time=9.179 ms
84 bytes from 10.1.20.254 icmp_seq=4 ttl=255 time=21.008 ms
84 bytes from 10.1.20.254 icmp_seq=5 ttl=255 time=4.545 ms
```
```
[robot-045@web1 ~]$ ping 10.1.30.254
PING 10.1.30.254 (10.1.30.254) 56(84) bytes of data.
64 bytes from 10.1.30.254: icmp_seq=1 ttl=255 time=12.4 ms
64 bytes from 10.1.30.254: icmp_seq=2 ttl=255 time=6.68 ms
64 bytes from 10.1.30.254: icmp_seq=3 ttl=255 time=6.70 ms

--- 10.1.30.254 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 6.677/8.604/12.433/2.707 ms
```
- en ajoutant une route vers les réseaux, ils peuvent se ping entre eux
  - ajoutez une route par défaut sur les VPCS
```
PC1> ip 10.1.10.1 255.255.255.0 10.1.10.254
Checking for duplicate address...
PC1 : 10.1.10.1 255.255.255.0 gateway 10.1.10.
```
```
PC2> ip 10.1.10.2 255.255.255.0 10.1.10.254
Checking for duplicate address...
PC2 : 10.1.10.2 255.255.255.0 gateway 10.1.10.254
```
```
adm1> ip 10.1.20.1 255.255.255.0 10.1.20.254
Checking for duplicate address...
adm1 : 10.1.20.1 255.255.255.0 gateway 10.1.20.254
```
  - ajoutez une route par défaut sur la machine virtuelle
```
[robot-045@web1 ~]$ip route add default via 10.1.10.254
```
  - testez des `ping` entre les réseaux
```
PC1> ping 10.1.10.2
84 bytes from 10.1.10.2 icmp_seq=1 ttl=64 time=0.914 ms
84 bytes from 10.1.10.2 icmp_seq=2 ttl=64 time=0.988 ms
84 bytes from 10.1.10.2 icmp_seq=3 ttl=64 time=1.482 ms
84 bytes from 10.1.10.2 icmp_seq=4 ttl=64 time=1.317 ms
84 bytes from 10.1.10.2 icmp_seq=5 ttl=64 time=1.098 ms

PC1> ping 10.1.20.1
84 bytes from 10.1.20.1 icmp_seq=1 ttl=63 time=42.543 ms
84 bytes from 10.1.20.1 icmp_seq=2 ttl=63 time=20.376 ms
84 bytes from 10.1.20.1 icmp_seq=3 ttl=63 time=22.563 ms
84 bytes from 10.1.20.1 icmp_seq=4 ttl=63 time=13.191 ms
84 bytes from 10.1.20.1 icmp_seq=5 ttl=63 time=21.461 ms

PC1> ping 10.1.30.1
84 bytes from 10.1.30.1 icmp_seq=1 ttl=63 time=15.754 ms
84 bytes from 10.1.30.1 icmp_seq=2 ttl=63 time=17.774 ms
84 bytes from 10.1.30.1 icmp_seq=3 ttl=63 time=15.542 ms
84 bytes from 10.1.30.1 icmp_seq=4 ttl=63 time=15.876 ms
84 bytes from 10.1.30.1 icmp_seq=5 ttl=63 time=21.984 ms
```
```
PC2> ping 10.1.10.1
84 bytes from 10.1.10.1 icmp_seq=1 ttl=64 time=0.583 ms
84 bytes from 10.1.10.1 icmp_seq=2 ttl=64 time=0.591 ms
84 bytes from 10.1.10.1 icmp_seq=3 ttl=64 time=1.081 ms
84 bytes from 10.1.10.1 icmp_seq=4 ttl=64 time=1.033 ms
84 bytes from 10.1.10.1 icmp_seq=5 ttl=64 time=1.401 ms

PC2> ping 10.1.20.1
84 bytes from 10.1.20.1 icmp_seq=1 ttl=63 time=20.754 ms
84 bytes from 10.1.20.1 icmp_seq=2 ttl=63 time=18.073 ms
84 bytes from 10.1.20.1 icmp_seq=3 ttl=63 time=18.707 ms
84 bytes from 10.1.20.1 icmp_seq=4 ttl=63 time=16.903 ms
84 bytes from 10.1.20.1 icmp_seq=5 ttl=63 time=19.777 ms

PC2> ping 10.1.30.1
84 bytes from 10.1.30.1 icmp_seq=1 ttl=63 time=38.047 ms
84 bytes from 10.1.30.1 icmp_seq=2 ttl=63 time=14.184 ms
84 bytes from 10.1.30.1 icmp_seq=3 ttl=63 time=14.577 ms
84 bytes from 10.1.30.1 icmp_seq=4 ttl=63 time=18.638 ms
84 bytes from 10.1.30.1 icmp_seq=5 ttl=63 time=22.123 ms
```
```
adm1> ping 10.1.10.1
84 bytes from 10.1.10.1 icmp_seq=1 ttl=63 time=20.548 ms
84 bytes from 10.1.10.1 icmp_seq=2 ttl=63 time=19.517 ms
84 bytes from 10.1.10.1 icmp_seq=3 ttl=63 time=11.078 ms
84 bytes from 10.1.10.1 icmp_seq=4 ttl=63 time=21.020 ms
84 bytes from 10.1.10.1 icmp_seq=5 ttl=63 time=15.864 ms

adm1> ping 10.1.10.2
84 bytes from 10.1.10.2 icmp_seq=1 ttl=63 time=19.431 ms
84 bytes from 10.1.10.2 icmp_seq=2 ttl=63 time=19.919 ms
84 bytes from 10.1.10.2 icmp_seq=3 ttl=63 time=16.141 ms
84 bytes from 10.1.10.2 icmp_seq=4 ttl=63 time=20.920 ms
84 bytes from 10.1.10.2 icmp_seq=5 ttl=63 time=21.054 ms

adm1> ping 10.1.30.1
84 bytes from 10.1.30.1 icmp_seq=1 ttl=63 time=41.393 ms
84 bytes from 10.1.30.1 icmp_seq=2 ttl=63 time=22.323 ms
84 bytes from 10.1.30.1 icmp_seq=3 ttl=63 time=21.475 ms
84 bytes from 10.1.30.1 icmp_seq=4 ttl=63 time=14.178 ms
84 bytes from 10.1.30.1 icmp_seq=5 ttl=63 time=15.016 ms
```
```
IOU1#show mac address-table
          Mac Address Table
-------------------------------------------
Vlan    Mac Address       Type        Ports
----    -----------       --------    -----
   1    c201.0714.0000    DYNAMIC     Et0/0
  10    0050.7966.6800    DYNAMIC     Et0/1
  10    0050.7966.6801    DYNAMIC     Et0/2
  10    c201.0714.0000    DYNAMIC     Et0/0
  20    0050.7966.6802    DYNAMIC     Et0/3
  20    c201.0714.0000    DYNAMIC     Et0/0
  30    0800.27f7.e009    DYNAMIC     Et1/0
  30    c201.0714.0000    DYNAMIC     Et0/0
Total Mac Addresses for this criterion: 8
```

---
# II. NAT

🤖 **Ajoutez le noeud Cloud à la topo**

- branchez à `eth1` côté Cloud
- côté routeur, il faudra récupérer un IP en DHCP 
```
R1#show ip interface brief
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            unassigned      YES NVRAM  up                    up
FastEthernet0/0.10         10.1.10.254     YES NVRAM  up                    up
FastEthernet0/0.20         10.1.20.254     YES NVRAM  up                    up
FastEthernet0/0.30         10.1.30.254     YES NVRAM  up                    up
FastEthernet0/1            unassigned      YES NVRAM  administratively down down
FastEthernet1/0            unassigned      YES NVRAM  administratively down down
FastEthernet2/0            unassigned      YES NVRAM  administratively down down
```
```
R1#conf t
R1(config)#interface fastEthernet 1/0
R1(config-if)#ip addr dhcp
R1(config-if)#no shutdown
R1#
```
```
R1#show ip interface brief
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            unassigned      YES NVRAM  up                    up
FastEthernet0/0.10         10.1.10.254     YES NVRAM  up                    up
FastEthernet0/0.20         10.1.20.254     YES NVRAM  up                    up
FastEthernet0/0.30         10.1.30.254     YES NVRAM  up                    up
FastEthernet0/1            unassigned      YES NVRAM  administratively down down
FastEthernet1/0            10.0.3.17       YES DHCP   up                    up
FastEthernet2/0            unassigned      YES NVRAM  administratively down down
```
- vous devriez pouvoir `ping 1.1.1.1`
```
R1#ping 1.1.1.1
Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 1.1.1.1, timeout is 2 seconds:
.!!!!
Success rate is 80 percent (4/5), round-trip min/avg/max = 16/25/44 ms
```

🤖 **Configurez le NAT**

- référez-vous à la section NAT du mémo Cisco
```
R1#conf t
R1(config)#interface fastEthernet 0/0
R1(config-if)#ip nat inside
R1(config-if)#exit
R1(config)#interface fastEthernet 1/0
R1(config-if)#ip nat outside
R1(config-if)#exit
R1(config)#interface fastEthernet 2/0
R1(config-if)#ip nat inside
R1(config-if)#exit
R1(config)#interface fastEthernet 0/1
R1(config-if)#ip nat inside
R1(config)#end
R1#
```
```
R1#conf t
R1(config)#access-list 1 permit any
R1(config)#exit
```
```
R1#conf t
R1(config)#ip nat inside source list 1 interface fastEthernet 1/0 overload
R1(config)#end
```
```
R1#show ip interface brief
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            unassigned      YES NVRAM  up                    up
FastEthernet0/0.10         10.1.10.254     YES NVRAM  up                    up
FastEthernet0/0.20         10.1.20.254     YES NVRAM  up                    up
FastEthernet0/0.30         10.1.30.254     YES NVRAM  up                    up
FastEthernet0/1            unassigned      YES NVRAM  administratively down down
FastEthernet1/0            10.0.3.17       YES DHCP   up                    up
FastEthernet2/0            unassigned      YES NVRAM  administratively down down
NVI0                       10.1.10.254     YES unset  up                    up
```

🤖 **Test**

- configurez l'utilisation d'un DNS
  - sur les VPCS
```
PC1> ip dns 1.1.1.1
PC1> ip dns 8.8.8.8
```
```
PC2> ip dns 1.1.1.1
PC2> ip dns 8.8.8.8
```
```
adm1> ip dns 1.1.1.1
adm1> ip dns 8.8.8.8
```
  - sur la machine Linux
```
[robot-045@web1 ~]$ sudo nano /etc/resolv.conf
```
```
[robot-045@web1 ~]$ cat /etc/resolv.conf
search servers.tp4
nameserver 1.1.1.1
nameserver 8.8.8.8
```
- vérifiez un `ping` vers un nom de domaine
```
PC1> ping example.com
example.com resolved to 93.184.216.34

84 bytes from 93.184.216.34 icmp_seq=1 ttl=52 time=152.490 ms
84 bytes from 93.184.216.34 icmp_seq=2 ttl=52 time=325.317 ms
84 bytes from 93.184.216.34 icmp_seq=3 ttl=52 time=127.566 ms
84 bytes from 93.184.216.34 icmp_seq=4 ttl=52 time=223.650 ms
84 bytes from 93.184.216.34 icmp_seq=5 ttl=52 time=130.095 ms
```
```
PC2> ping example.com
example.com resolved to 93.184.216.34

84 bytes from 93.184.216.34 icmp_seq=1 ttl=52 time=206.919 ms
84 bytes from 93.184.216.34 icmp_seq=2 ttl=52 time=228.369 ms
84 bytes from 93.184.216.34 icmp_seq=3 ttl=52 time=227.926 ms
84 bytes from 93.184.216.34 icmp_seq=4 ttl=52 time=118.673 ms
84 bytes from 93.184.216.34 icmp_seq=5 ttl=52 time=231.558 ms
```
```
PC3> ping example.com
example.com resolved to 93.184.216.34

84 bytes from 93.184.216.34 icmp_seq=1 ttl=52 time=212.000 ms
84 bytes from 93.184.216.34 icmp_seq=2 ttl=52 time=130.065 ms
84 bytes from 93.184.216.34 icmp_seq=3 ttl=52 time=134.054 ms
84 bytes from 93.184.216.34 icmp_seq=4 ttl=52 time=218.436 ms
84 bytes from 93.184.216.34 icmp_seq=5 ttl=52 time=229.810 ms
```
```
[robot-045@web1 ~]$ ping example.com
...
```

---
# III. Add a building

🤖  **Vous devez me rendre le `show running-config` de tous les équipements**

- de tous les équipements réseau
  - le routeur
```
R1#show running-config
Building configuration...

...
interface FastEthernet0/0
 no ip address
 ip nat inside
 ip virtual-reassembly
 duplex auto
 speed auto
!
interface FastEthernet0/0.10
 encapsulation dot1Q 10
 ip address 10.1.10.254 255.255.255.0
!
interface FastEthernet0/0.20
 encapsulation dot1Q 20
 ip address 10.1.20.254 255.255.255.0
!
interface FastEthernet0/0.30
 encapsulation dot1Q 30
 ip address 10.1.30.254 255.255.255.0
!
interface FastEthernet0/1
 no ip address
 ip nat inside
 ip virtual-reassembly
 shutdown
 duplex auto
 speed auto
!
interface FastEthernet1/0
 ip address dhcp
 ip nat outside
 ip virtual-reassembly
 duplex auto
 speed auto
!
interface FastEthernet2/0
 no ip address
 ip nat inside
 ip virtual-reassembly
 shutdown
 duplex auto
 speed auto
!
ip forward-protocol nd
!
!
no ip http server
no ip http secure-server
ip nat inside source list 1 interface FastEthernet1/0 overload
!
access-list 1 permit any
no cdp log mismatch duplex
...
end
```

  - les 3 switches
```
sw1#show running-config
Building configuration...

...
interface Ethernet0/0
 switchport trunk allowed vlan 10,20,30
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/1
 switchport trunk allowed vlan 10,20,30
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/2
 switchport trunk allowed vlan 10,20,30
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/3
!
interface Ethernet1/0
!
interface Ethernet1/1
!
interface Ethernet1/2
!
interface Ethernet1/3
!
interface Ethernet2/0
!
interface Ethernet2/1
!
interface Ethernet2/2
!
interface Ethernet2/3
!
interface Ethernet3/0
!
interface Ethernet3/1
!
interface Ethernet3/2
!
interface Ethernet3/3
!
interface Vlan1
 no ip address
 shutdown
!
ip forward-protocol nd
!
ip tcp synwait-time 5
ip http server
!
ip ssh server algorithm encryption aes128-ctr aes192-ctr aes256-ctr
ip ssh client algorithm encryption aes128-ctr aes192-ctr aes256-ctr
...
end
```

```
sw2#show running-config
Building configuration...

...
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/1
 switchport access vlan 10
 switchport mode access
!
interface Ethernet0/2
 switchport access vlan 10
 switchport mode access
!
interface Ethernet0/3
 switchport access vlan 20
 switchport mode access
!
interface Ethernet1/0
 switchport access vlan 30
 switchport mode access
!
interface Ethernet1/1
!
interface Ethernet1/2
!
interface Ethernet1/3
!
interface Ethernet2/0
!
interface Ethernet2/1
!
interface Ethernet2/2
!
interface Ethernet2/3
!
interface Ethernet3/0
!
interface Ethernet3/1
!
interface Ethernet3/2
!
interface Ethernet3/3
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan10
 no ip address
!
interface Vlan20
 no ip address
 shutdown
!
interface Vlan30
 no ip address
 shutdown
!
ip forward-protocol nd
!
ip tcp synwait-time 5
ip http server
!
ip ssh server algorithm encryption aes128-ctr aes192-ctr aes256-ctr
ip ssh client algorithm encryption aes128-ctr aes192-ctr aes256-ctr
...
end
```

```
sw3#show running-config
Building configuration...

...
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/1
 switchport access vlan 10
 switchport mode access
!
interface Ethernet0/2
 switchport access vlan 10
 switchport mode access
!
interface Ethernet0/3
 switchport access vlan 10
 switchport mode access
!
interface Ethernet1/0
 switchport access vlan 10
 switchport mode access
!
interface Ethernet1/1
!
interface Ethernet1/2
!
interface Ethernet1/3
!
interface Ethernet2/0
!
interface Ethernet2/1
!
interface Ethernet2/2
!
interface Ethernet2/3
!
interface Ethernet3/0
!
interface Ethernet3/1
!
interface Ethernet3/2
!
interface Ethernet3/3
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan10
 no ip address
!
ip forward-protocol nd
!
ip tcp synwait-time 5
ip http server
!
ip ssh server algorithm encryption aes128-ctr aes192-ctr aes256-ctr
ip ssh client algorithm encryption aes128-ctr aes192-ctr aes256-ctr
...
end
```

🤖  **Mettre en place un serveur DHCP dans le nouveau bâtiment**

- conf
```
[robot-045@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf

default-lease-time 600;

max-lease-time 7200;

authoritative;

subnet 10.1.10.0 netmask 255.255.255.0 {
  range 10.1.10.100 10.1.10.200;
  option routers 10.1.10.254;
  option domain-name-servers 1.1.1.1;
}
```

🤖  **Vérification**

- un client récupère une IP en DHCP
```
NAME   IP/MASK              GATEWAY           MAC                LPORT  RHOST:PORT
PC5    10.1.10.102/24       10.1.10.254       00:50:79:66:68:05  20026  127.0.0.1:20027
       fe80::250:79ff:fe66:6805/64
```
- il peut ping le serveur Web
```
PC5> ping 10.1.30.1

84 bytes from 10.1.30.1 icmp_seq=1 ttl=63 time=46.939 ms
84 bytes from 10.1.30.1 icmp_seq=2 ttl=63 time=18.710 ms
84 bytes from 10.1.30.1 icmp_seq=3 ttl=63 time=20.191 ms
84 bytes from 10.1.30.1 icmp_seq=4 ttl=63 time=22.923 ms
84 bytes from 10.1.30.1 icmp_seq=5 ttl=63 time=15.434 ms
```
- il peut ping `1.1.1.1`
```
PC5> ping 1.1.1.1

1.1.1.1 icmp_seq=1 timeout
1.1.1.1 icmp_seq=2 timeout
84 bytes from 1.1.1.1 icmp_seq=3 ttl=62 time=18.257 ms
84 bytes from 1.1.1.1 icmp_seq=4 ttl=62 time=16.201 ms
84 bytes from 1.1.1.1 icmp_seq=5 ttl=62 time=18.343 ms
```
- il peut ping `ynov.com`
```
PC5> ping ynov.com
ynov.com resolved to 172.67.74.226

84 bytes from 172.67.74.226 icmp_seq=1 ttl=53 time=42.789 ms
84 bytes from 172.67.74.226 icmp_seq=2 ttl=53 time=37.383 ms
84 bytes from 172.67.74.226 icmp_seq=3 ttl=53 time=39.296 ms
84 bytes from 172.67.74.226 icmp_seq=4 ttl=53 time=42.257 ms
84 bytes from 172.67.74.226 icmp_seq=5 ttl=53 time=50.920 ms
```