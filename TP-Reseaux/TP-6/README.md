# TP6 : STP, OSPF, bigger infra

## Sommaire

- [TP6 : STP, OSPF, bigger infra](#tp6--stp-ospf-bigger-infra)
  - [Sommaire](#sommaire)
  - [0. Setup](#0-setup)
  - [I. STP](#i-stp)
  - [II. OSPF](#ii-ospf)
  - [III. DHCP relay](#iii-dhcp-relay)

## 0. Setup

## I. STP

🤖 **Configurer STP sur les 3 switches**

```
sw1#show spanning-tree vlan 10 | include Interface |Et0/0|Et0/1|Et0/2
Interface           Role Sts Cost      Prio.Nbr Type
Et0/0               Desg FWD 100       128.1    P2p
Et0/1               Desg FWD 100       128.2    P2p
Et0/2               Desg FWD 100       128.3    P2p
```
```
sw2#show spanning-tree vlan 10 | include Interface |Et0/0|Et0/1|Et0/2
Interface           Role Sts Cost      Prio.Nbr Type
Et0/0               Root FWD 100       128.1    P2p
Et0/1               Desg FWD 100       128.2    P2p
Et0/2               Desg FWD 100       128.3    P2p
```
```
sw3#show spanning-tree vlan 10 | include Interface |Et0/0|Et0/1|Et0/2
Interface           Role Sts Cost      Prio.Nbr Type
Et0/0               Altn BLK 100       128.1    P2p
Et0/1               Root FWD 100       128.2    P2p
Et0/2               Desg FWD 100       128.3    P2p
```

🤖 **Altérer le spanning-tree** en désactivant un port

- désactiver juste un port de un switch pour provoquer la mise à jour de STP
```
sw3#conf t
sw3(config)#interface Ethernet0/1
sw3(config-if)#shutdown
sw3(config-if)#end
```
- `show spanning-tree` pour voir la diff
```
sw3#show spanning-tree vlan 10 | include Interface |Et0/0|Et0/1|Et0/2
Interface           Role Sts Cost      Prio.Nbr Type
Et0/0               Root FWD 100       128.1    P2p
Et0/2               Desg FWD 100       128.3    P2p
```

🤖 **Altérer le spanning-tree** en modifiant le coût d'un lien

- modifier le coût d'un lien existant pour modifier l'arbre spanning-tree
```
sw3#conf t
sw3(config)#interface Ethernet0/1
sw3(config-if)#spanning-tree cost 50
sw3(config-if)#end
```
```
sw3#show spanning-tree vlan 10 | include Interface |Et0/0|Et0/1|Et0/2
Interface           Role Sts Cost      Prio.Nbr Type
Et0/0               Desg LIS 100       128.1    P2p
Et0/1               Root FWD 50        128.2    P2p
Et0/2               Desg FWD 100       128.3    P2p
```

🤖 **Capture STP**

- 📂 fichier tp6_stp.pcapng 
[Lien vers capture](./captures/tp6_stp.pcapng)

## II. OSPF

🤖 **Montez la topologie**

```
R1#show ip interface brief
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            10.6.41.1       YES NVRAM  up                    up
FastEthernet0/1            10.6.21.1       YES NVRAM  up                    up
FastEthernet1/0            10.6.13.1       YES NVRAM  up                    up
FastEthernet2/0            10.6.3.254      YES NVRAM  up                    up
```
```
R2#show ip interface brief
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            10.6.21.2       YES NVRAM  up                    up
FastEthernet0/1            10.6.23.2       YES NVRAM  up                    up
FastEthernet1/0            10.6.52.2       YES NVRAM  up                    up
FastEthernet2/0            unassigned      YES NVRAM  administratively down down
```
```
R3#show ip interface brief
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            10.6.13.2       YES NVRAM  up                    up
FastEthernet0/1            10.6.23.1       YES NVRAM  up                    up
FastEthernet1/0            unassigned      YES NVRAM  administratively down down
FastEthernet2/0            unassigned      YES NVRAM  administratively down down
```
```
R4#show ip interface brief
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            10.6.1.254      YES NVRAM  up                    up
FastEthernet0/1            10.6.2.254      YES NVRAM  up                    up
FastEthernet1/0            10.6.41.2       YES NVRAM  up                    up
FastEthernet2/0            unassigned      YES NVRAM  administratively down down
```
```
R5#conf t
R5(config)#interface fastethernet1/0
R5(config-if)#ip nat outside
R5(config-if)#exit
R5(config)#interface fastethernet0/0
R5(config-if)#ip nat inside
R5(config-if)#end
```

```
R5#conf t
R5(config)#interface fastethernet1/0
R5(config-if)#ip address dhcp
R5(config-if)#no shut
R5(config-if)#end
```
```
R5#show ip interface brief
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            10.6.52.1       YES NVRAM  up                    up
FastEthernet0/1            192.168.122.29  YES DHCP   up                    up
FastEthernet1/0            unassigned      YES NVRAM  administratively down down
FastEthernet2/0            unassigned      YES NVRAM  administratively down down
NVI0                       10.6.52.1       YES unset  up                    up
```

```
R1#conf t
R1(config)#router ospf 1
R1(config-router)#router-id 1.1.1.1
R1(config-router)#network 10.6.3.0 0.0.0.255 area 2
R1(config-router)#network 10.6.41.0 0.0.0.3 area 3
R1(config-router)#network 10.6.13.0 0.0.0.3 area 0
R1(config-router)#network 10.6.21.0 0.0.0.3 area 0
R1(config-router)#end

```
```
R2#conf t
R2(config)#router ospf 1
R2(config-router)#router-id 2.2.2.2
R2(config-router)#network 10.6.21.0 0.0.0.3 area 0
R2(config-router)#network 10.6.23.0 0.0.0.3 area 0
R2(config-router)#network 10.6.52.0 0.0.0.3 area 1
R2(config-router)#end

```
```
R3#conf t
R3(config)#router ospf 1
R3(config-router)#router-id 3.3.3.3
R3(config-router)#network 10.6.13.0 0.0.0.3 area 0
R3(config-router)#network 10.6.23.0 0.0.0.3 area 0
R3(config-router)#end
```
```
R4#conf t
R4(config)#router ospf 1
R4(config-router)#router-id 4.4.4.4
R4(config-router)#network 10.6.1.0 0.0.0.255 area 3
R4(config-router)#network 10.6.2.0 0.0.0.255 area 3
R4(config-router)#network 10.6.41.0 0.0.0.3 area 3
R4(config-router)#end
```
```
R5#conf t
R5(config)#router ospf 1
R5(config-router)#router-id 5.5.5.5
R5(config-router)#network 10.6.52.0 0.0.0.3 area 1
R5(config-router)#end
```

🌞 **Configurer OSPF sur tous les routeurs**

- tous les routeurs doivent partager tous les réseaux auxquels ils sont connectés
- un petit `show running-config` où vous enlevez ce que vous n'avez pas tapé pour le rendu !

```
R1#show running-config
Building configuration...

interface FastEthernet0/0
 ip address 10.6.41.1 255.255.255.252
 duplex auto
 speed auto
!
interface FastEthernet0/1
 ip address 10.6.21.1 255.255.255.252
 duplex auto
 speed auto
!
interface FastEthernet1/0
 ip address 10.6.13.1 255.255.255.252
 duplex auto
 speed auto
!
interface FastEthernet2/0
 ip address 10.6.3.254 255.255.255.0
 duplex auto
 speed auto
!
router ospf 1
 router-id 1.1.1.1
 log-adjacency-changes
 network 10.6.3.0 0.0.0.3 area 2
 network 10.6.3.0 0.0.0.255 area 2
 network 10.6.13.0 0.0.0.3 area 0
 network 10.6.21.0 0.0.0.3 area 0
 network 10.6.41.0 0.0.0.3 area 3
!
```
```
R2#show running-config
Building configuration...

interface FastEthernet0/0
 ip address 10.6.21.2 255.255.255.252
 duplex auto
 speed auto
!
interface FastEthernet0/1
 ip address 10.6.23.2 255.255.255.252
 duplex auto
 speed auto
!
interface FastEthernet1/0
 ip address 10.6.52.2 255.255.255.252
 duplex auto
 speed auto
!
interface FastEthernet2/0
 no ip address
 shutdown
 duplex auto
 speed auto
!
router ospf 1
 router-id 2.2.2.2
 log-adjacency-changes
 network 10.6.21.0 0.0.0.3 area 0
 network 10.6.23.0 0.0.0.3 area 0
 network 10.6.52.0 0.0.0.3 area 1
!
```
```
R3#show running-config
Building configuration...

interface FastEthernet0/0
 ip address 10.6.13.2 255.255.255.252
 duplex auto
 speed auto
!
interface FastEthernet0/1
 ip address 10.6.23.1 255.255.255.252
 duplex auto
 speed auto
!
interface FastEthernet1/0
 no ip address
 shutdown
 duplex auto
 speed auto
!
interface FastEthernet2/0
 no ip address
 shutdown
 duplex auto
 speed auto
!
router ospf 1
 router-id 3.3.3.3
 log-adjacency-changes
 network 10.6.13.0 0.0.0.3 area 0
 network 10.6.23.0 0.0.0.3 area 0
!
```
```
R4#show running-config
Building configuration...

interface FastEthernet0/0
 ip address 10.6.1.254 255.255.255.0
 duplex auto
 speed auto
!
interface FastEthernet0/1
 ip address 10.6.2.254 255.255.255.0
 duplex auto
 speed auto
!
interface FastEthernet1/0
 ip address 10.6.41.2 255.255.255.252
 duplex auto
 speed auto
!
interface FastEthernet2/0
 no ip address
 shutdown
 duplex auto
 speed auto
!
router ospf 1
 router-id 4.4.4.4
 log-adjacency-changes
 network 10.6.1.0 0.0.0.255 area 3
 network 10.6.2.0 0.0.0.255 area 3
 network 10.6.41.0 0.0.0.3 area 3
!
```
```
R5#show running-config
Building configuration...

interface FastEthernet0/0
 ip address 10.6.52.1 255.255.255.252
 ip nat inside
 ip virtual-reassembly
 duplex auto
 speed auto
!
interface FastEthernet0/1
 ip address dhcp
 ip nat outside
 ip virtual-reassembly
 duplex auto
 speed auto
!
interface FastEthernet1/0
 no ip address
 shutdown
 duplex auto
 speed auto
!
interface FastEthernet2/0
 no ip address
 shutdown
 duplex auto
 speed auto
!
router ospf 1
 router-id 5.5.5.5
 log-adjacency-changes
 network 10.6.52.0 0.0.0.3 area 1
 default-information originate always
!
```

- et un `show ip ospf neighbor` + `show ip route` sur chaque routeur

```
R1#show ip ospf neighbor
Neighbor ID     Pri   State           Dead Time   Address         Interface
2.2.2.2           1   FULL/DR         00:00:31    10.6.21.2       FastEthernet0/1
3.3.3.3           1   FULL/DR         00:00:39    10.6.13.2       FastEthernet1/0
4.4.4.4           1   FULL/DR         00:00:31    10.6.41.2       FastEthernet0/0
R1#show ip route
Codes: C - connected, S - static, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2
       i - IS-IS, su - IS-IS summary, L1 - IS-IS level-1, L2 - IS-IS level-2
       ia - IS-IS inter area, * - candidate default, U - per-user static route
       o - ODR, P - periodic downloaded static route

Gateway of last resort is 10.6.21.2 to network 0.0.0.0

     10.0.0.0/8 is variably subnetted, 8 subnets, 2 masks
C       10.6.13.0/30 is directly connected, FastEthernet1/0
O       10.6.1.0/24 [110/20] via 10.6.41.2, 00:25:04, FastEthernet0/0
O       10.6.2.0/24 [110/20] via 10.6.41.2, 00:25:04, FastEthernet0/0
C       10.6.3.0/24 is directly connected, FastEthernet2/0
C       10.6.21.0/30 is directly connected, FastEthernet0/1
O       10.6.23.0/30 [110/11] via 10.6.13.2, 00:25:14, FastEthernet1/0
C       10.6.41.0/30 is directly connected, FastEthernet0/0
O IA    10.6.52.0/30 [110/11] via 10.6.21.2, 00:25:06, FastEthernet0/1
O*E2 0.0.0.0/0 [110/1] via 10.6.21.2, 00:24:51, FastEthernet0/1
```
```
R2#show ip ospf neighbor
Neighbor ID     Pri   State           Dead Time   Address         Interface
3.3.3.3           1   FULL/DR         00:00:31    10.6.23.1       FastEthernet0/1
1.1.1.1           1   FULL/BDR        00:00:30    10.6.21.1       FastEthernet0/0
5.5.5.5           1   FULL/DR         00:00:36    10.6.52.1       FastEthernet1/0
R2#show ip route
Codes: C - connected, S - static, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2
       i - IS-IS, su - IS-IS summary, L1 - IS-IS level-1, L2 - IS-IS level-2
       ia - IS-IS inter area, * - candidate default, U - per-user static route
       o - ODR, P - periodic downloaded static route

Gateway of last resort is 10.6.52.1 to network 0.0.0.0

     10.0.0.0/8 is variably subnetted, 8 subnets, 2 masks
O       10.6.13.0/30 [110/11] via 10.6.21.1, 00:25:08, FastEthernet0/0
O IA    10.6.1.0/24 [110/30] via 10.6.21.1, 00:25:08, FastEthernet0/0
O IA    10.6.2.0/24 [110/30] via 10.6.21.1, 00:25:08, FastEthernet0/0
O IA    10.6.3.0/24 [110/11] via 10.6.21.1, 00:25:08, FastEthernet0/0
C       10.6.21.0/30 is directly connected, FastEthernet0/0
C       10.6.23.0/30 is directly connected, FastEthernet0/1
O IA    10.6.41.0/30 [110/20] via 10.6.21.1, 00:25:09, FastEthernet0/0
C       10.6.52.0/30 is directly connected, FastEthernet1/0
O*E2 0.0.0.0/0 [110/1] via 10.6.52.1, 00:24:59, FastEthernet1/0
```
```
R3#show ip ospf neighbor
Neighbor ID     Pri   State           Dead Time   Address         Interface
2.2.2.2           1   FULL/BDR        00:00:36    10.6.23.2       FastEthernet0/1
1.1.1.1           1   FULL/BDR        00:00:33    10.6.13.1       FastEthernet0/0
R3#show ip route
Codes: C - connected, S - static, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2
       i - IS-IS, su - IS-IS summary, L1 - IS-IS level-1, L2 - IS-IS level-2
       ia - IS-IS inter area, * - candidate default, U - per-user static route
       o - ODR, P - periodic downloaded static route

Gateway of last resort is 10.6.23.2 to network 0.0.0.0

     10.0.0.0/8 is variably subnetted, 8 subnets, 2 masks
C       10.6.13.0/30 is directly connected, FastEthernet0/0
O IA    10.6.1.0/24 [110/30] via 10.6.13.1, 00:25:10, FastEthernet0/0
O IA    10.6.2.0/24 [110/30] via 10.6.13.1, 00:25:10, FastEthernet0/0
O IA    10.6.3.0/24 [110/11] via 10.6.13.1, 00:25:20, FastEthernet0/0
O       10.6.21.0/30 [110/20] via 10.6.23.2, 00:25:20, FastEthernet0/1
                     [110/20] via 10.6.13.1, 00:25:10, FastEthernet0/0
C       10.6.23.0/30 is directly connected, FastEthernet0/1
O IA    10.6.41.0/30 [110/20] via 10.6.13.1, 00:25:21, FastEthernet0/0
O IA    10.6.52.0/30 [110/11] via 10.6.23.2, 00:25:21, FastEthernet0/1
O*E2 0.0.0.0/0 [110/1] via 10.6.23.2, 00:24:51, FastEthernet0/1
```
```
R4#show ip ospf neighbor
Neighbor ID     Pri   State           Dead Time   Address         Interface
1.1.1.1           1   FULL/BDR        00:00:35    10.6.41.1       FastEthernet1/0
R4#show ip route
Codes: C - connected, S - static, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2
       i - IS-IS, su - IS-IS summary, L1 - IS-IS level-1, L2 - IS-IS level-2
       ia - IS-IS inter area, * - candidate default, U - per-user static route
       o - ODR, P - periodic downloaded static route

Gateway of last resort is 10.6.41.1 to network 0.0.0.0

     10.0.0.0/8 is variably subnetted, 8 subnets, 2 masks
O IA    10.6.13.0/30 [110/2] via 10.6.41.1, 00:25:19, FastEthernet1/0
C       10.6.1.0/24 is directly connected, FastEthernet0/0
C       10.6.2.0/24 is directly connected, FastEthernet0/1
O IA    10.6.3.0/24 [110/2] via 10.6.41.1, 00:25:19, FastEthernet1/0
O IA    10.6.21.0/30 [110/11] via 10.6.41.1, 00:25:19, FastEthernet1/0
O IA    10.6.23.0/30 [110/12] via 10.6.41.1, 00:25:19, FastEthernet1/0
C       10.6.41.0/30 is directly connected, FastEthernet1/0
O IA    10.6.52.0/30 [110/12] via 10.6.41.1, 00:25:17, FastEthernet1/0
O*E2 0.0.0.0/0 [110/1] via 10.6.41.1, 00:25:02, FastEthernet1/0
```
```
R5#show ip ospf neighbor
Neighbor ID     Pri   State           Dead Time   Address         Interface
2.2.2.2           1   FULL/BDR        00:00:32    10.6.52.2       FastEthernet0/0
R5#show ip route
Codes: C - connected, S - static, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2
       i - IS-IS, su - IS-IS summary, L1 - IS-IS level-1, L2 - IS-IS level-2
       ia - IS-IS inter area, * - candidate default, U - per-user static route
       o - ODR, P - periodic downloaded static route

Gateway of last resort is 192.168.122.1 to network 0.0.0.0

C    192.168.122.0/24 is directly connected, FastEthernet0/1
     10.0.0.0/8 is variably subnetted, 8 subnets, 2 masks
O IA    10.6.13.0/30 [110/21] via 10.6.52.2, 00:25:05, FastEthernet0/0
O IA    10.6.1.0/24 [110/40] via 10.6.52.2, 00:25:05, FastEthernet0/0
O IA    10.6.2.0/24 [110/40] via 10.6.52.2, 00:25:05, FastEthernet0/0
O IA    10.6.3.0/24 [110/21] via 10.6.52.2, 00:25:05, FastEthernet0/0
O IA    10.6.21.0/30 [110/20] via 10.6.52.2, 00:25:05, FastEthernet0/0
O IA    10.6.23.0/30 [110/20] via 10.6.52.2, 00:25:07, FastEthernet0/0
O IA    10.6.41.0/30 [110/30] via 10.6.52.2, 00:25:07, FastEthernet0/0
C       10.6.52.0/30 is directly connected, FastEthernet0/0
S*   0.0.0.0/0 [254/0] via 192.168.122.1

```

🤖 **Test**

```
waf.tp6.b1> ping 10.6.1.253
84 bytes from 10.6.1.253 icmp_seq=1 ttl=64 time=6.683 ms
84 bytes from 10.6.1.253 icmp_seq=2 ttl=64 time=3.506 ms
84 bytes from 10.6.1.253 icmp_seq=3 ttl=64 time=3.360 ms
84 bytes from 10.6.1.253 icmp_seq=4 ttl=64 time=3.445 ms
84 bytes from 10.6.1.253 icmp_seq=5 ttl=64 time=3.664 ms

waf.tp6.b1> ping 10.6.2.11
10.6.2.11 icmp_seq=1 timeout
84 bytes from 10.6.2.11 icmp_seq=2 ttl=63 time=16.352 ms
84 bytes from 10.6.2.11 icmp_seq=3 ttl=63 time=12.766 ms
84 bytes from 10.6.2.11 icmp_seq=4 ttl=63 time=17.460 ms
84 bytes from 10.6.2.11 icmp_seq=5 ttl=63 time=17.451 ms

waf.tp6.b1> ping 10.6.3.11
10.6.3.11 icmp_seq=1 timeout
84 bytes from 10.6.3.11 icmp_seq=2 ttl=62 time=33.627 ms
84 bytes from 10.6.3.11 icmp_seq=3 ttl=62 time=35.325 ms
84 bytes from 10.6.3.11 icmp_seq=4 ttl=62 time=41.021 ms
84 bytes from 10.6.3.11 icmp_seq=5 ttl=62 time=38.885 ms
```
```
waf.tp6.b1> ping 10.6.41.1
84 bytes from 10.6.41.1 icmp_seq=1 ttl=254 time=31.981 ms
84 bytes from 10.6.41.1 icmp_seq=2 ttl=254 time=30.685 ms
84 bytes from 10.6.41.1 icmp_seq=3 ttl=254 time=14.518 ms
84 bytes from 10.6.41.1 icmp_seq=4 ttl=254 time=33.636 ms
84 bytes from 10.6.41.1 icmp_seq=5 ttl=254 time=32.003 ms

waf.tp6.b1> ping 10.6.21.1
84 bytes from 10.6.21.1 icmp_seq=1 ttl=254 time=28.113 ms
84 bytes from 10.6.21.1 icmp_seq=2 ttl=254 time=20.535 ms
84 bytes from 10.6.21.1 icmp_seq=3 ttl=254 time=26.182 ms
84 bytes from 10.6.21.1 icmp_seq=4 ttl=254 time=20.697 ms
84 bytes from 10.6.21.1 icmp_seq=5 ttl=254 time=34.590 ms

waf.tp6.b1> ping 10.6.23.2
84 bytes from 10.6.23.2 icmp_seq=1 ttl=253 time=61.649 ms
84 bytes from 10.6.23.2 icmp_seq=2 ttl=253 time=47.128 ms
84 bytes from 10.6.23.2 icmp_seq=3 ttl=253 time=62.084 ms
84 bytes from 10.6.23.2 icmp_seq=4 ttl=253 time=38.362 ms
84 bytes from 10.6.23.2 icmp_seq=5 ttl=253 time=65.843 ms

waf.tp6.b1> ping 10.6.52.1
84 bytes from 10.6.52.1 icmp_seq=1 ttl=252 time=55.509 ms
84 bytes from 10.6.52.1 icmp_seq=2 ttl=252 time=70.347 ms
84 bytes from 10.6.52.1 icmp_seq=3 ttl=252 time=68.936 ms
84 bytes from 10.6.52.1 icmp_seq=4 ttl=252 time=58.904 ms
84 bytes from 10.6.52.1 icmp_seq=5 ttl=252 time=47.895 ms
```
```
waf.tp6.b1> ping 1.1.1.1
84 bytes from 1.1.1.1 icmp_seq=1 ttl=124 time=92.789 ms
84 bytes from 1.1.1.1 icmp_seq=2 ttl=124 time=87.383 ms
84 bytes from 1.1.1.1 icmp_seq=3 ttl=124 time=89.296 ms
84 bytes from 1.1.1.1 icmp_seq=4 ttl=124 time=92.257 ms
84 bytes from 1.1.1.1 icmp_seq=5 ttl=124 time=100.920 ms
```

- 📂 fichier tp6_ospf.pcapng
[Lien vers capture](./captures/tp6_ospf.pcapng)

## III. DHCP relay

🤖 **Configurer un serveur DHCP** sur `dhcp.tp6.b1`

```
[robot-045@dhcp ~]$sudo dnf install -y dhcp
```
```
[robot-045@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
# create new

default-lease-time 600;
max-lease-time 7200;
authoritative;

subnet 10.6.1.0 netmask 255.255.255.0 {
  range 10.6.1.100 10.6.1.200;
  option routers 10.6.1.254;
  option domain-name-servers 1.1.1.1;
}

subnet 10.6.3.0 netmask 255.255.255.0 {
  range 10.6.3.100 10.6.3.200;
  option routers 10.6.3.254;
  option domain-name-servers 1.1.1.1;
}
EOF;
```
```
[robot-045@dhcp ~]$ sudo systemctl status dhcpd.service
● dhcpd.service - DHCPv4 Server Daemon
   Loaded: loaded (/usr/lib/systemd/system/dhcpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2023-12-07 11:16:36 CET; 2min 28s ago
     Docs: man:dhcpd(8)
           man:dhcpd.conf(5)
 Main PID: 988 (dhcpd)
   Status: "Dispatching packets..."
    Tasks: 1 (limit: 4698)
   Memory: 9.6M
   CGroup: /system.slice/dhcpd.service
           └─988 /usr/sbin/dhcpd -f -cf /etc/dhcp/dhcpd.conf -user dhcpd -group dhcpd --no-pid
```

🤖 **Configurer un DHCP relay sur la passerelle de John**

```
waf.tp6.b1> ip dhcp
DDORA IP 10.6.1.101/24 GW 10.6.1.254

john.tp6.b1> ip dhcp
DDORA IP 10.6.3.101/24 GW 10.6.3.254
```
```
[robot-045@dhcp ~]journalctl -xeu dhcpd.service
Dec 07 12:12:24 dhcp.tp6.b2 dhcpd[986]: DHCPDISCOVER from 00:58:79:66:68:00 (waf.tp6.b11) via enp0s3
Dec 07 12:12:24 dhcp.tp6.b2 dhcpd[986]: DHCPOFFER on 10.6.1.100 to 00:50:79:66:68:00 (waf.tp6.b11) via enp0s3
Dec 07 12:12:26 dhcp.tp6.b2 dhcpd[986]: DHCPREQUEST for 10.6.1.100 (10.6.1.253) from 00:50:79:66:68:00 (waf.tp6.b11) via enp0s3
Dec 07 12:12:26 dhcp.tp6.b2 dhcpd[986]: DHCPACK on 10.6.1.100 to 00:50:79:66:68:00 (waf.tp6.b11) via enp0s3
Dec 07 12:12:51 dhcp.tp6.b2 dhcpd[986]: reuse_lease: lease age 25 (secs) under 25% threshold, reply with unaltered, existing lease for 10.6.1.100
Dec 07 12:12:51 dhcp.tp6.b2 dhcpd[986]: DHCPDISCOVER from 00:58:79:66:68:00 (waf.tp6.b11) via enp0s3
Dec 07 12:12:51 dhcp.tp6.b2 dhcpd[986]: DHCPOFFER on 10.6.1.100 to 00:50:79:66:68:00 (waf.tp6.b11) via enp0s3
Dec 07 12:12:52 dhcp.tp6.b2 dhcpd[986]: reuse_lease: lease age 26 (secs) under 25% threshold, reply with unaltered, existing lease for 10.6.1.100
Dec 07 12:12:52 dhcp.tp6.b2 dhcpd[986]: DHCPREQUEST for 10.6.1.100 (10.6.1.253) from 00:58:79:66:68:00 (waf.tp6.b11) via enp0s3
Dec 07 12:12:52 dhcp.tp6.b2 dhcpd[986]: DHCPACK on 10.6.1.100 to 00:50:79:66:68:00 (waf.tp6.b11) via enp0s3
Dec 07 12:12:59 dhcp.tp6.b2 dhcpd[986]: DHCPDISCOVER from 00:50:79:66:68:02 (john.tp6.b11) via 10.6.3.254
Dec 07 12:12:59 dhcp.tp6.b2 dhcpd[986]: DHCPOFFER on 10.6.3.100 to 00:50:79:66:68:02 (john.tp6.b11) via 10.6.3.254
Dec 07 12:13:00 dhcp.tp6.b2 dhcpd[986]: DHCPREQUEST for 10.6.3.100 (10.6.1.253) from 00:50:79:66:68:02 (john.tp6.b11) via 10.6.3.254
Dec 07 12:13:00 dhcp.tp6.b2 dhcpd[986]: DHCPACK on 10.6.3.100 to 00:50:79:66:68:02 (john.tp6.b11) via 10.6.3.254
```
