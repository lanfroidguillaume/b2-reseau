# TP1 : Maîtrise réseau du poste

- [TP1 : Maîtrise réseau du poste](#tp1--maîtrise-réseau-du-poste)
- [I. Basics](#i-basics)
- [II. Go further](#ii-go-further)
- [III. Le requin](#iii-le-requin)

# I. Basics
---
🤖 **Carte réseau WiFi**

1. l'adresse MAC de votre carte WiFi
``` 
PS C:\Users\lanfr> Get-NetAdapter -Name "Wi-Fi" | Format-List -Property "MacAddress"
MacAddress : 2C-6D-C1-5E-41-6A
```
2. l'adresse IP de votre carte WiFi
```
ipconfig /all 
[...] 
Adresse IPv4. . . . . . . . . . . . . .: 10.33.76.217(préféré)
[...] 
```
3. le masque de sous-réseau du réseau LAN auquel vous êtes connectés en WiFi
-en notation CIDR,
```
10.33.64.0/20
```
-en notation décimale
```
ipconfig /all
[...] 
Masque de sous-réseau. . . . . . . . . : 255.255.240.0
[...] 
```

---
🤖 **Déso pas déso**

| LAN | Broadcast | IP disponibles |
|:-:   |:-:    |:-:    |
| 10.33.64.0 | 10.33.79.255 | 4094 |

```
 2^12 = 4096
 4096 - 2 = 4094 
 il y a 4094 adresses IP disponibles dans ce réseau.
```

---
🤖 **Hostname**

- déterminer le hostname de votre PC
```
PS C:\Users\lanfr> hostname
LAPTOP-01DSJ4MN
```

---
🤖 **Passerelle du réseau**

- l'adresse IP de la passerelle du réseau
```
ipconfig /all
[...] 
Passerelle par défaut. . . . . . . . . : 10.33.79.254
[...] 
```
- l'adresse MAC de la passerelle du réseau
```
PS C:\Users\lanfr> arp -a | Select-String "10.33.79.254"
  10.33.79.254          7c-5a-1c-d3-d8-76     dynamique
```

---
🤖 **Serveur DHCP et DNS**

- l'adresse IP du serveur DHCP qui vous a filé une IP
```
ipconfig /all
[...] 
Serveur DHCP . . . . . . . . . . . . . : 10.33.79.254
[...] 
```
- l'adresse IP du serveur DNS que vous utilisez quand vous allez sur internet
```
ipconfig /all
[...] 
 Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
[...] 
```

---
🤖 **Table de routage**

- dans votre table de routage, laquelle est la route par défaut
```
PS C:\Users\lanfr> route print | Select-String "0.0.0.0"
          0.0.0.0          0.0.0.0     10.33.79.254     10.33.76.217     30
```

---
# II. Go further

🤖️ **Hosts ?**

- faites en sorte que pour votre PC, le nom `b2.hello.vous` corresponde à l'IP `1.1.1.1`
```
PS C:\Users\lanfr> echo 1.1.1.1 b2.hello.vous C:\Windows\System32\drivers\etc\host
1.1.1.1
b2.hello.vous
C:\Windows\System32\drivers\etc\host
PS C:\Users\lanfr> type C:\Windows\System32\drivers\etc\hosts | Select-String "1.1.1.1"
1.1.1.1 b2.hello.vous
```
- prouvez avec un `ping b2.hello.vous` que ça ping bien `1.1.1.1`
```
PS C:\Users\lanfr> ping b2.hello.vous

Envoi d’une requête 'ping' sur b2.hello.vous [1.1.1.1] avec 32 octets de données :
Réponse de 1.1.1.1 : octets=32 temps=11 ms TTL=57
Réponse de 1.1.1.1 : octets=32 temps=10 ms TTL=57
Réponse de 1.1.1.1 : octets=32 temps=10 ms TTL=57
Réponse de 1.1.1.1 : octets=32 temps=10 ms TTL=57

Statistiques Ping pour 1.1.1.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 10ms, Maximum = 11ms, Moyenne = 10ms
```

---
🤖 **G omater une vidéo youtube et déterminer, pendant qu'elle tourne...**

| IP du serveur |   port serveur |   port PC local |
|:-:   |:-:    |:-:    |
| 77.136.192.79 | 443 | 53337 |

- 📂 fichier `video.pcap`
[Lien vers capture ARP](./TP/TP-1/captures/video.pcap)

---
🤖 **Requêtes DNS**

- à quelle adresse IP correspond le nom de domaine `www.ynov.com`
```
PS C:\Users\lanfr> nslookup www.ynov.com
Serveur :   dns.google
Address:  8.8.8.8

Réponse ne faisant pas autorité :
Nom :    www.ynov.com
Addresses:  2606:4700:20::681a:be9
          2606:4700:20::ac43:4ae2
          2606:4700:20::681a:ae9
          172.67.74.226
          104.26.10.233
          104.26.11.233
```
- à quel nom de domaine correspond l'IP `174.43.238.89`
```
PS C:\Users\lanfr> nslookup 174.43.238.89
Serveur :   dns.google
Address:  8.8.8.8

Nom :    89.sub-174-43-238.myvzw.com
Address:  174.43.238.89
```

---
🤖 **Hop hop hop**

- par combien de machines vos paquets passent quand vous essayez de joindre `www.ynov.com`
```
PS C:\Users\lanfr> tracert www.ynov.com

Détermination de l’itinéraire vers www.ynov.com [172.67.74.226]
avec un maximum de 30 sauts :

  1     4 ms     1 ms     1 ms  10.33.79.254
  2     3 ms     2 ms     2 ms  145.117.7.195.rev.sfr.net [195.7.117.145]
  3     2 ms     2 ms     3 ms  237.195.79.86.rev.sfr.net [86.79.195.237]
  4     3 ms     2 ms     4 ms  196.224.65.86.rev.sfr.net [86.65.224.196]
  5    10 ms    11 ms    10 ms  12.148.6.194.rev.sfr.net [194.6.148.12]
  6    11 ms    10 ms    10 ms  12.148.6.194.rev.sfr.net [194.6.148.12]
  7    11 ms     9 ms    10 ms  141.101.67.48
  8    10 ms    11 ms    12 ms  172.71.124.4
  9    11 ms    11 ms    10 ms  172.67.74.226

Itinéraire déterminé.
```

---
🤖 **IP publique**

- l'adresse IP publique de la passerelle du réseau (le routeur d'YNOV donc si vous êtes dans les locaux d'YNOV quand vous faites le TP)
```
https://www.mon-ip.com/info-adresse-ip.php
```
```
195.7.117.146
```

---
🤖 **Scan réseau**

- combien il y a de machines dans le LAN auquel vous êtes connectés
```
23
```
```
PS C:\Users\lanfr> arp -a | Select-String "10.33."

Interface : 10.33.76.217 --- 0x10
  10.33.65.104          d0-c6-37-79-3d-b7     dynamique
  10.33.65.112          0c-54-15-d9-32-3f     dynamique
  10.33.65.188          32-fd-2f-fc-81-4a     dynamique
  10.33.65.223          8c-85-90-53-24-b3     dynamique
  10.33.66.10           28-11-a8-30-cf-05     dynamique
  10.33.66.17           38-fc-98-8d-b4-a9     dynamique
  10.33.68.36           c4-bd-e5-79-f4-aa     dynamique
  10.33.68.74           c0-b6-f9-70-dc-08     dynamique
  10.33.69.0            f8-89-d2-7c-36-7d     dynamique
  10.33.69.3            e0-2b-e9-77-20-bd     dynamique
  10.33.69.149          cc-f9-e4-c5-99-c3     dynamique
  10.33.70.183          34-6f-24-93-79-ad     dynamique
  10.33.70.209          00-93-37-9d-52-7e     dynamique
  10.33.71.128          a0-29-42-24-b0-3f     dynamique
  10.33.75.150          b0-a4-60-63-a1-87     dynamique
  10.33.75.160          30-03-c8-37-39-ff     dynamique
  10.33.75.207          f8-ac-65-0f-5b-9f     dynamique
  10.33.76.226          48-e7-da-29-d8-b3     dynamique
  10.33.77.12           f4-4e-e3-c1-ae-6c     dynamique
  10.33.77.183          f0-9e-4a-51-d3-49     dynamique
  10.33.77.230          e0-2b-e9-a9-de-c2     dynamique
  10.33.79.254          7c-5a-1c-d3-d8-76     dynamique
  10.33.79.255          ff-ff-ff-ff-ff-ff     statique
```

# III. Le requin
---
🤖 **Capture ARP**

- 📂 fichier `arp.pcap`
[Lien vers capture ARP](./TP/TP-1/captures/arp.pcap)

---
🤖 **Capture DNS**

- 📂 fichier `dns.pcap`
[Lien vers capture DNS](./TP/TP-1/captures/dns.pcap)
- vous effectuerez la requête DNS en ligne de commande
```
PS C:\Users\lanfr> nslookup example.com
Serveur :   dns.google
Address:  8.8.8.8

Réponse ne faisant pas autorité :
Nom :    example.com
Addresses:  2606:2800:220:1:248:1893:25c8:1946
          93.184.216.34
```

---
🤖 **Capture TCP**

- 📂 fichier `tcp.pcap`
[Lien vers capture TCP](./TP/TP-1/captures/dns.pcap)
---